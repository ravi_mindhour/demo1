<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Manage Employee</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Manage Employee</h3>
</div>
</div>
<div id="loading" style="position:absolute;width:100%;height:100%; top:0; z-index:9999; background:#000;display:none; opacity:.5;">

<img src="<?php echo base_url("assets/loading.gif"); ?>" style="width:20%; margin-left:40%; margin-top:10%;" />

</div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
       <!-- <select id="selTemplate">
        </select>-->
        
            <div class="">
               <div class="table-responsive">
                <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                           <?php $k=0;
                           	for($i=0; $i<count($headers); $i++)
                           	{
                           		/*$th_name = "";
                           		if(isset($datum[$i]) and $datum[$i]["business_attribute_id"] == $headers[$i]["id"])
                           		{
                           			$th_name = $datum[$i]["display_name_override"];
                           		}

                           		if($th_name == "")
                           		{
									$th_name = $headers[$i]["display_name"];
                           		}
                           		echo  "<th>".$th_name."</th>";*/
								
								echo  "<th>".$headers[$i]["display_name_override"]."</th>";
                           	}
                           ?>
                        </tr>
                    </thead>
                    <tbody id="tbl_body">
                   
                     <?php $k = 0;	//echo  count($datum)/count($headers);
						for($i = 0; $i < (count($datum)/count($headers)); $i++)
						{
							echo "<tr>";
							
							for($j=0; $j< count($headers); $j++)
							{							
								if(isset($datum[$k]))
								{
									echo "<td class=hidden-xs>".$datum[$k]["uploaded_value"]."</td>";
								}
								$k++;
							}
							echo "</tr>";
						} ?>
					 
                    </tbody>
                   </table>
                   
                   <?php if($is_enable_approve_btn){ ?>
                   <div class="m-t-lg mob-center">
                   <!--<input style="visibility:hidden;" type="text" id="txtTemplate" placeholder="Enter Template Name" />
                   <label> Save as template </label>
                    <input type="checkBox" class="btn  "  id="chkTemplate" onclick="showTemplateText(this);" />-->
                     <a href="<?php echo site_url("update-approvel/".$upload_id); ?>"><input type="button" class="btn btn-twitter m-b-sm add-btn" value="Approved" id="btnSave" /></a>
                                                     </div>
                  <?php } ?>
                    
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</div><!-- Row -->
</div>