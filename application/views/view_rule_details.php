<div class="page-breadcrumb">
	<ol class="breadcrumb container">
		<li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
		<li class="active">View Rule</li>
	</ol>
</div>
<div class="page-title">
	<div class="container">
		<h3>View Rule</h3>
	</div>
</div>
<div id="main-wrapper" class="container">
	<div class="row">
		<div class="col-md-12">
        	<div class="mb20">
                <div class="panel panel-white">
                    <?= $this->session->flashdata('message'); ?>      
                    <div class="panel-body">
                        <div class="rule-form">
                            <div class="form-group">
                            	<div class="row">
                                	<div class="col-sm-6">
                                    	<div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputEmail3" class="control-label">Cycle Name</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-input">
                                                    <select class="form-control" name="ddl_performance_cycle" id="ddl_performance_cycle" disabled="disabled">
                                                    <?php 
                                                        foreach($performance_cycle_list as $row)
                                                        {									
                                                            echo "<option value=".$row["id"].">".$row["name"]."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                      	</div>
                                   	</div>
                               	</div>
                            </div>
                            <div id="dv_partial_page_data">
                            	<div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="inputPassword" class="control-label">Include Inactive</label>            
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="form-control" name="ddl_include_inactive" disabled="disabled">
                                                    	<option value="">Select</option>
                                                        <option value="yes" <?php if($rule_dtls["include_inactive"]=="yes"){echo 'selected="selected"';} ?> >Yes</option>
                                                        <option value="no" <?php if($rule_dtls["include_inactive"]=="no"){echo 'selected="selected"';} ?> >No</option>
                                                    </select>
                                                </div> 
                                            </div>           
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Performnace Based Hikes</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="ddl_performnace_based_hikes" name="ddl_performnace_based_hikes" disabled="disabled">
                                                    <option value="">Select</option>
                                                    <option value="yes" <?php if($rule_dtls["performnace_based_hike"]=="yes"){echo 'selected="selected"';} ?>>Yes</option>
                                                    <option value="no" <?php if($rule_dtls["performnace_based_hike"]=="no"){echo 'selected="selected"';} ?>>No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="perfom-y">
                                        <div class="row">
                                            <div class="col-sm-12" id="dv_rating_list_yes">
                                                <?php 
												$rating_dtls = json_decode($rule_dtls["performnace_based_hike_ratings"],true);
                                                if($rule_dtls["performnace_based_hike"] == "yes")
                                                {													
                                                    foreach ($rating_dtls as $key => $value) 
                                                    {
                                                        $key_arr =  explode(CV_CONCATENATE_SYNTAX, $key);
														
														echo '<div class="form-group"><div class="row"><div class="col-sm-6">';
														echo '<label class="control-label">'.$key_arr[1].'</label></div>';
														echo '<div class="col-sm-6">
														<input type="text" name="txt_rating[]" class="form-control" value="'.$value.'" readonly="readonly" />
														</div></div>
														</div>';
														
                                                    }			
                                                }
                                                else
                                                {
                                                    	echo '<div class="form-group"><div class="row"><div class="col-sm-6">';
														echo '<label class="control-label">Rating</label></div>';
														echo '<div class="col-sm-6">
														<input type="text" name="txt_rating" class="form-control" value="'.$rating_dtls["all"].'" readonly="readonly" />
														</div></div>
														</div>';
                                                }
												?>
                                            </div>
                                        </div>        
                                    </div>                                    
                                    
                                </div>  
                                </div>
                                
                                <div class="row">          
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Comparative ratio to be applied after performance based merit increase</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="ddl_comparative_ratio" name="ddl_comparative_ratio" disabled="disabled">
                                                    <option value="">Select</option>
                                                    <option value="yes" <?php if($rule_dtls["comparative_ratio"]=="yes"){echo 'selected="selected"';} ?>>Yes</option>
                                                    <option value="no" <?php if($rule_dtls["comparative_ratio"]=="no"){echo 'selected="selected"';} ?>>No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="perfom-y">
                                        <div class="row">
                                            <div class="col-sm-12" id="dv_apply_comparative_ratio_yes">
                                            <?php
												$comparative_ratio_dtls = json_decode($rule_dtls["comparative_ratio_calculations"],true);

												if($rule_dtls["comparative_ratio"] == "yes")
												{
													foreach ($comparative_ratio_dtls as $key => $value) 
													{
														$key_arr =  explode(CV_CONCATENATE_SYNTAX, $key);
														$val_arr =  explode(CV_CONCATENATE_SYNTAX, $value);
														
														echo '<div class="form-group"><div class="row"><div class="col-sm-6">';
														echo '<label class="control-label">'.$key_arr[1].'</label></div>';
														echo '<div class="col-sm-6">
															 <select class="form-control" name="ddl_market_salary_comparative_ratio[]" disabled="disabled">
															 <option>'.$val_arr[1].'</option>
															 </select></div></div></div>';
													}			
												}
												else
												{
													$val_arr =  explode(CV_CONCATENATE_SYNTAX, $comparative_ratio_dtls["all"]);
													echo '<div class="form-group"><div class="row"><div class="col-sm-6">';
													echo '<label class="control-label">Comparative Ratio</label></div>';
													echo '<div class="col-sm-6">
														 <select class="form-control" name="ddl_market_salary_comparative_ratio[]" disabled="disabled">
														 <option>'.$val_arr[1].'</option>
														 </select></div></div></div>';
												}
											?>
                                            </div>
                                        </div>        
                                    </div>                                
                                </div>
                                </div>
                                
                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Comparative ratio ranges (CRR)</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Range</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                    	<?php														
														$crr_arr = json_decode($rule_dtls["comparative_ratio_range"], true);
														foreach($crr_arr as $row)
														{			
														echo '<div class="form-group rv1" id="dv_new_ratio_range_row">
															<div class="row">
																<div class="col-sm-8">
																	<input type="text" name="txt_crr[]" class="form-control" value="'.$row["max"].'" readonly="readonly">
																</div>
															</div>
														</div>';
														}														
														?>														                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                </div> 
                                
                                <div class="row">  
                                <div class="col-sm-6">    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Prorated Increase</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="form-control" name="ddl_prorated_increase" disabled="disabled">
                                                    <option value="yes" <?php if($rule_dtls["prorated_increase"]=="yes"){echo 'selected="selected"';} ?>>Yes</option>
                                                    <option value="no" <?php if($rule_dtls["prorated_increase"]=="no"){echo 'selected="selected"';} ?>>No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div>   
                                </div>
                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Manager's discretionary increase </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <span style="font-weight:bold; font-size:20px; margin-left:50px;">-</span> <br>
                                                <input type="text" name="txt_manager_discretionary_decrease" class="form-control" value="<?php echo $rule_dtls["Manager_discretionary_decrease"]; ?>" readonly="readonly"/>
                                            </div>
                                            <div class="col-sm-3">
                                                <span style="font-weight:bold; font-size:20px; margin-left:50px;">+</span><br>
                                                <input type="text" name="txt_manager_discretionary_increase" class="form-control" value="<?php echo $rule_dtls["Manager_discretionary_increase"]; ?>" readonly="readonly"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                </div>                                
                            </div> 
                            <div id="dv_partial_page_data_2">
                                <div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Ratings</th>
                    <th>Market Benchmark</th>
                    <?php $i=0; $crr_arr = json_decode($rule_dtls["comparative_ratio_range"], true);
                        foreach($crr_arr as $row)
                        { 
                    ?>
                    <th> 
                        <?php 
                            if($i==0)
                            { 
                                echo "<".$row["max"];
                            }
                            elseif(($i+1)==count($crr_arr))
                            {
                            echo " > ".$row["max"];
                            }
                            else
                            {
                                echo $row["min"]." < ".$row["max"];
                            }
                        ?>CR 
                    </th>
                    <?php 
                            $i++; 
                        } 
                    ?>
                    <th style="background-color: #22baa0; color: #fff">Max Hike</th>
                    <th style="background-color: #22baa0; color: #fff">Recently promoted max salary increase</th>
                   
                </tr>
            </thead>
            <tbody>
            <?php //echo "<pre>";print_r(json_decode($rule_dtls["crr_percent_values"],true));
                $crr_percent_values_arr = json_decode($rule_dtls["crr_percent_values"],true);
				$max_hike_arr = json_decode($rule_dtls["Overall_maximum_age_increase"],true);
				$recently_promoted_max_salary_arr = json_decode($rule_dtls["if_recently_promoted"],true);
				$j=0; $crr_arr1 = json_decode($rule_dtls["comparative_ratio_calculations"],true);

                foreach($crr_arr1 as $key => $val)
                { 
            ?>
            <tr>
                <td>
                <?php $rating_name_arr =explode(CV_CONCATENATE_SYNTAX, $key); 
                    if($rule_dtls["comparative_ratio"] == 'yes')
                    {
                        echo $rating_name_arr[1];
                    }
                    else
                    {
                        echo $rating_name_arr[0];
                    }
                ?>
                    
                </td>
                <td><?php $market_salalry_arr =explode(CV_CONCATENATE_SYNTAX, $val); echo $market_salalry_arr[1]; ?></td>
                <?php 
                    $i = 0;
                    foreach($crr_arr as $row)
                    { 
                ?>                		
                <td><input type="text" class="form-control" name="txt_range_val<?php echo $i; ?>[]" value="<?php echo $crr_percent_values_arr[$i][$j]; ?>" readonly="readonly"></td>      
                <?php 
                    $i++; 
                    } 
                ?>
                <td><input type="text" class="form-control" name="txt_max_hike[]" value="<?php echo $max_hike_arr[$j]; ?>" readonly="readonly"></td> 
                <td><input type="text" class="form-control" name="txt_recently_promoted_max_salary_increase[]" value="<?php echo $recently_promoted_max_salary_arr[$j]; ?>" readonly="readonly"></td> 
            </tr>
            <?php 
                $j++;} 
            ?>
        </tbody>
    </table>
    </div>
</div>		
                                <div class="row">
                                    <div class="col-sm-6">            
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Standard Promotion increase</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="txt_standard_promotion_increase" class="form-control" value="<?php echo $rule_dtls["standard_promotion_increase"]; ?>" readonly="readonly"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Oveall budget allocated for salary increase for the team</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="form-control" name="ddl_overall_budget" disabled="disabled">
                                                        <option value="">Select</option>
                                                        <option value="No limit" <?php if($rule_dtls["overall_budget"]=="No limit"){echo 'selected="selected"';} ?>>No limit</option>
                                                        <option value="Automated locked" <?php if($rule_dtls["overall_budget"]=="Automated locked"){echo 'selected="selected"';} ?>>Automated locked</option>
                                                        <option value="Automated but x% can exceed" <?php if($rule_dtls["overall_budget"]=="Automated but x% can exceed"){echo 'selected="selected"';} ?>>Automated but x% can exceed</option>
                                                        <option value="Manual" <?php if($rule_dtls["overall_budget"]=="Manual"){echo 'selected="selected"';} ?>>Manual</option>
                                                    </select>
                                                    <br />
                                                    <?php if($rule_dtls["overall_budget"]=="Automated locked" or $rule_dtls["overall_budget"]=="Automated but x% can exceed"){?>
                                                    <input type="text" class="form-control" name="txt_budget_amt" id="txt_budget_amt" placeholder="Budget Amount" value="<?php echo $rule_dtls["budget_amount"]; ?>" readonly="readonly"> 
                                                    <br />
                                                   <?php } if($rule_dtls["overall_budget"]=="Automated but x% can exceed"){?>
                                                    <input type="text" class="form-control" name="txt_budget_percent" id="txt_budget_percent" placeholder="Budget %" value="<?php echo $rule_dtls["budget_percent"]; ?>" readonly="readonly"> 
													<?php } ?>	
                                                </div>
                                            </div>
                                        </div>
                                    </div>            
                                </div>
                                
                                <?php if($is_enable_approve_btn){ ?>
                                    <div class="row">
                                        <div class="col-sm-2 col-sm-offset-4">
                                            <div class="submit-btn">
                                                <a href="<?php echo site_url("update-rule-approvel/".$upload_id); ?>"><input type="button" class="btn btn-twitter m-b-sm add-btn" value="Approved" id="btnSave" /></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div> 
                        </div>                 
                    </div>
                </div>
          	</div>
		</div>
	</div>
</div>