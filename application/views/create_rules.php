<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="page-breadcrumb">
	<ol class="breadcrumb container">
		<li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
		<li class="active">Create Rules</li>
	</ol>
</div>
<div class="page-title">
	<div class="container">
		<h3>Create Rules</h3>
	</div>
</div>
<div id="main-wrapper" class="container">
	<div class="row">
		<div class="col-md-12">
        	<div class="mb20">
                <div class="panel panel-white">
                    <?= $this->session->flashdata('message'); ?>      
                    <div class="panel-body">
                        <div class="rule-form">
                            <div class="form-group">
                            	<div class="row">
                                	<div class="col-sm-6">
                                    	<div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputEmail3" class="control-label">Cycle Name</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-input">
                                                    <select class="form-control" name="ddl_performance_cycle" id="ddl_performance_cycle" required onchange="get_performance_cycle_dtls(this.value);">
                                                    <?php 
                                                        foreach($performance_cycle_list as $row)
                                                        {									
                                                            echo "<option value=".$row["id"].">".$row["name"]."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                      	</div>
                                   	</div>
                               	</div>
                            </div>
                            <div id="dv_partial_page_data"></div> 
                            <div id="dv_partial_page_data_2"></div> 
                        </div>                 
                    </div>
                </div>
          	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	get_performance_cycle_dtls($("#ddl_performance_cycle").val());
	function get_performance_cycle_dtls(val)
	{
		if(val)
		{
		 	$.post("<?php echo site_url("rules/get_performance_cycle_dtls");?>",{pid:val}, function(data)
		 	{
				if(data)
				{
					$("#dv_partial_page_data").html(data);
				}
			}); 
		}
		else
		{
			$("#dv_partial_page_data").html("");
		}
	}
</script>



<script type="text/javascript">



function show_hide_budget_dv(val)

{
    $("#txt_budget_amt").prop('required',false);
    $("#txt_budget_percent").prop('required',false);
    $("#txt_budget_amt").hide();

    $("#txt_budget_percent").hide();

    if(val=='Automated locked')
    {
        $("#txt_budget_amt").prop('required',true);
        $("#txt_budget_amt").show();      
    }
    else if(val=='Automated but x% can exceed')
    {
        $("#txt_budget_amt").prop('required',true);
        $("#txt_budget_percent").prop('required',true);
        $("#txt_budget_amt").show();
        $("#txt_budget_percent").show();
    }



}



function addAnotherRow()

{    

    var cl2 = $("#dv_new_ratio_range_row").clone()

    .find("input:text").val("").end(); //alert(c12);

    cl2.find("#dv_btn_dlt").html('<button type="button" class="btn btn-danger" onclick="$(this).closest(\'.rv1\').remove();">DELETE</button>');

   //cl2.append('<button type="button" class="btn btn-danger" onclick="$(this).closest(\'.rv1\').remove();">DELETE</button>');  

    cl2.insertBefore("#dv_btn_add_row");    



}



function set_rules(frm)

{

    var form=$("#"+frm.id);

    $.ajax({

        type:"POST",

        url:form.attr("action"),

        data:form.serialize(),

    

        success: function(response)

        {

            if(response==2)

            {

                window.location.href= "<?php echo site_url("view-rule-budget/".$performance_cycle_list[0]["upload_id"]);?>";

            } 

            else

            {

                //get_performance_cycle_dtls($("#ddl_performance_cycle").val());

                 $.post("<?php echo site_url("rules/get_performance_cycle_dtls");?>",{pid:$("#ddl_performance_cycle").val()}, function(data)

                 {

                   if(data)

                   {

                        $("#dv_frm_1_btn").html('');

                        $("#dv_partial_page_data_2").html(data);

                   }

                }); 

            }

        }

    });

    

    return false;

}



function manage_performnace_based_hikes(val)
{
    $("#txt_rating1").prop('required',false);
    $("#dv_rating_list_yes").hide();

    $("#dv_rating_list_no").hide();

    $("#dv_rating_list_yes").html("");

    if(val=='yes')

    {

        var pid = $("#ddl_performance_cycle").val();

        $.post("<?php echo site_url("rules/get_ratings_list");?>",{pid:pid,txt_name:'rating'}, function(data)

        {

            if(data)

            {

                $("#dv_rating_list_yes").html(data);

                $("#dv_rating_list_yes").show();

            }
            else
            {
                $("#ddl_performnace_based_hikes").val('no');
                $("#txt_rating1").prop('required',true);
                $("#dv_rating_list_no").show();
                $("#common_popup_for_alert").html('<div align="left" style="color:blue;" id="notify"><span><b>No rating available.</b></span></div>');
                $.magnificPopup.open({
                    items: {
                        src: '#common_popup_for_alert'
                    },
                    type: 'inline'
                });
                setTimeout(function(){$('#common_popup_for_alert').magnificPopup('close');},2000);
            }

        });         

    }

    else if(val=='no')
    {
        $("#txt_rating1").prop('required',true);
        $("#dv_rating_list_no").show();

    }

}



function manage_comparative_ratio(val)
{
    $("#dv_apply_comparative_ratio_yes").hide();
    $("#dv_apply_comparative_ratio_no").hide();
    $("#dv_apply_comparative_ratio_yes").html('');
    if(val=='yes')
    {
        var pid = $("#ddl_performance_cycle").val();
        $.post("<?php echo site_url("rules/get_ratings_list");?>",{pid:pid,txt_name:'comparative_ratio'}, function(data)
        {
            if(data)
            {
                $("#dv_apply_comparative_ratio_yes").html(data);
                $("#dv_apply_comparative_ratio_yes").show();
            }
            else
            {
                $("#ddl_comparative_ratio").val('no');
                manage_comparative_ratio($("#ddl_comparative_ratio").val());
                $("#txt_rating1").prop('required',true);
                
                $("#common_popup_for_alert").html('<div align="left" style="color:blue;" id="notify"><span><b>No rating available.</b></span></div>');
                $.magnificPopup.open({
                    items: {
                        src: '#common_popup_for_alert'
                    },
                    type: 'inline'
                });
                setTimeout(function(){$('#common_popup_for_alert').magnificPopup('close');},2000);
            }
        });        
    }
    else if(val=='no')
    {
         var pid = $("#ddl_performance_cycle").val();
        $.post("<?php echo site_url("rules/get_comparative_ratio_element_for_no");?>",{pid:pid}, function(data)
        {
            if(data)
            {
                $("#ddl_comparative_ratio1").html(data);
                $("#dv_apply_comparative_ratio_no").show();
            }
        }); 
    }
}

function validate_onkeyup(that)
{
    that.value = that.value.replace(/[^0-9.]/g,'');
    if(((that.value).split('.')).length>2)
    {
        var arr = (that.value).split('.');
        that.value=arr[0]+"."+arr[1];
    }
}
function validate_onblure(that)
{
    that.value = that.value.replace(/[^0-9.]/g,'');
    if(((that.value).split('.')).length>2)
    {
        var arr = (that.value).split('.');
        that.value=arr[0]+"."+arr[1];
    }
    if((that.value) && Number(that.value)<=0)
    {
        that.value="0";
    }
}
</script>

