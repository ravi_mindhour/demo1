
<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="index.php">Dashboard</a></li>
        <li class="active">Manage City</li>
    </ol>
</div>
<div class="page-title">
    <div class="container">
        <h3>Manage City</h3>
    </div>
</div>
<div id="main-wrapper" class="container">

	<div class="row mb20">
    	<div class="col-md-6 col-md-offset-3">
            <div class="panel panel-white">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group my-form">
                            <label for="inputEmail3" class="col-sm-3 control-label">City Name</label>
                            <div class="col-sm-9 form-input">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group my-form">
                            <label for="inputEmail3" class="col-sm-3 control-label">Select Country</label>
                            <div class="col-sm-9 form-input">
                                <select class="js-states form-control" tabindex="-1" style="width: 100%">
                                    <optgroup label="Select Country">
                                        <option value="">India</option>
                                        <option value="">America</option>
                                        <option value="">England</option>
                                        <option value="">Canda</option>
                                        <option value="">Finland</option>
                                        <option value="">Russia</option>
                                        <option value="">Pakistan</option>
                                        <option value="">Sri Lanka</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group my-form">
                            <label for="inputPassword3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9 form-input">
                                <select class="js-states form-control" tabindex="-1" style="width: 100%">
                                    <option selected>Active</option>
                                    <option>Inactive</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="">
                            <div class="col-sm-offset-3 col-sm-9 mob-center">
                                <button type="submit" class="btn btn-twitter">Add City</button>
                                <button class="btn btn-twitter">Cancle</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
        	<div class="panel panel-white">
                <div class="">
                  <div class="mailbox-content">
                    <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                      <thead>
                        <tr>
                          <th class="hidden-xs" width="4%"><input type="checkbox" class="check-mail-all"></th>
                          <th class="hidden-xs" width="5%">S.No</th>
                          <th>Country</th>
                          <th>City</th>
                          <th class="hidden-xs" style="text-align:center;" width="7%">Status</th>
                          <th class="hidden-xs" style="text-align:center;" width="5%">Edit</th>
                          <th class="hidden-xs" style="text-align:center;" width="5%">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">01</td>
                          <td>India</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">02</td>
                          <td>America</td>
                          <td>Washinton DC</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/inactive.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">03</td>
                          <td>England</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">04</td>
                          <td>Canda</td>
                          <td>Philips</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">05</td>
                          <td>Finland</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">06</td>
                          <td>Russia</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">07</td>
                          <td>Pakistan</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">08</td>
                          <td>Sri Lanka</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">09</td>
                          <td>India</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">10</td>
                          <td>America</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">11</td>
                          <td>England</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                        <tr>
                          <td class="hidden-xs"><input type="checkbox" class="checkbox-mail"></td>
                          <td class="hidden-xs">12</td>
                          <td>Canda</td>
                          <td>Delhi</td>
                          <td class="hidden-xs" align="center"><img src="assets/images/active.png" alt=""></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-edit"></span></a></td>
                          <td class="hidden-xs" align="center"><a href="#"><span class="fa fa-trash-o"></span></a></td>
                        </tr>
                      </tbody>
                    </table>
                    <div class="m-t-lg mob-center"> 
                    <a type="button" class="btn btn-twitter m-b-sm mail-hidden-options hidden-xs" href="#">Active</a>
                    <a type="button" class="btn btn-twitter m-b-sm mail-hidden-options hidden-xs" href="#">Inactive</a>
                    <a type="button" class="btn btn-twitter m-b-sm mail-hidden-options hidden-xs" href="#">Delete</a>
                  </div>
                </div>
          </div>
        
        
            
        </div>
    </div><!-- Row -->
</div>
                
              