<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Add Business Attributes</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <div class="row">
        <div class="col-sm-5 mob-center">
            <h3>Add Business Attributes</h3>
        </div>

        <div class="col-sm-7 mob-center">
            <div class="pull-right">
                <a href="<?php echo site_url("business-attributes"); ?>"><button class="btn btn-success" type="button">Business Attributes List</button></a>
            </div>
        </div>
    </div>
</div>

<div id="main-wrapper" class="container">
<div class="row mb20">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-white">
        <?= $this->session->flashdata('message'); ?>      
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="">
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Display Name</label>
                        <div class="col-sm-9 form-input">
                            <input id="txt_name" name="txt_display_name" type="text" class="form-control" required="required" maxlength="50" value="<?php if(isset($business_attribute_dtl)){ echo $business_attribute_dtl[0]["display_name"];} ?>">
                        </div>
                    </div>                   
                    
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Module Name</label>
                        <div class="col-sm-9 form-input">
                            <select id="ddl_module_name" name="ddl_module_name" class="js-states form-control" tabindex="-1" style="width: 100%" required="required">
                                <option  value="N/A">N/A</option> 
                                <option  value="Business_unit" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "Business_unit"){ echo 'selected="selected"';} ?>>Business Unit</option>
                                <option  value="country" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "country"){ echo 'selected="selected"';} ?>>Country</option>
                                <option  value="designation" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "designation"){ echo 'selected="selected"';} ?>>Designation</option>
                                <option  value="email" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "email"){ echo 'selected="selected"';} ?>>Email</option> 
                                <option  value="employee_name" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "employee_name"){ echo 'selected="selected"';} ?>>Employee Name</option> 
                                <option  value="first_approver" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "first_approver"){ echo 'selected="selected"';} ?>>First Approver</option>
                                <option  value="hod_approver" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "hod_approver"){ echo 'selected="selected"';} ?>>HOD Approver</option>

                                <option  value="increment_applied_on" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "increment_applied_on"){ echo 'selected="selected"';} ?>>Increment Applied On</option>
                                <option  value="market_salary" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "market_salary"){ echo 'selected="selected"';} ?>>Market Salary</option>                                
                                <option  value="rating" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "rating"){ echo 'selected="selected"';} ?>>Rating</option>    
                                <option  value="salary" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "salary"){ echo 'selected="selected"';} ?>>Salary</option>  
                                <option  value="second_approver" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "second_approver"){ echo 'selected="selected"';} ?>>Second Approver</option>  
                                <option  value="third_approver" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["module_name"] == "third_approver"){ echo 'selected="selected"';} ?>>Third Approver</option>  
                            </select>
                        </div>
                    </div>
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Attribute For</label>
                        <div class="col-sm-9 form-input">
                            <select id="ddl_type" name="ddl_type" class="js-states form-control" tabindex="-1" style="width: 100%" required="required">                               
                                <option  value="1">Employee</option>
                                <option  value="2" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["type"] == "2"){ echo 'selected="selected"';} ?>>Salary</option>
                            </select>
                        </div>
                    </div> 
                    
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Data Type</label>
                        <div class="col-sm-9 form-input">
                             <select id="ddl_data_type" name="ddl_data_type" class="js-states form-control" tabindex="-1" style="width: 100%" required="required">
                                <option  value="">Select</option>
                                <?php foreach($data_type_list as $row){?>
                                <option  value="<?php echo $row["code"]; ?>"  <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["data_type_code"] == $row["code"]){ echo 'selected="selected"';} ?>><?php echo $row["display_text"]; ?></option>
                                <?php } ?>                               
                            </select>
                        </div>
                    </div>    

                    <div class="form-group my-form">
                        <label for="inputPassword3" class="col-sm-3 control-label">Is Required</label>
                        <div class="col-sm-9 form-input">
                            Yes <input type="radio" name="rb_is_required" value="1" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["is_required"] == "1"){ echo 'checked="checked"';} ?>/>
                            No <input type="radio" name="rb_is_required" value="0" <?php if(isset($business_attribute_dtl) and $business_attribute_dtl[0]["is_required"] == "0"){ echo 'checked="checked"';} elseif(!isset($business_attribute_dtl)){echo 'checked="checked"';} ?>/>
                        </div>
                    </div>                           
                    
                    <div class="">
                        <div class="col-sm-offset-3 col-sm-9 mob-center">
                            <input type="submit" id="btnAdd" value="Add" class="btn btn-success" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

