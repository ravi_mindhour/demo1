<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Add Country</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Add Country</h3>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-white">
        <?= $this->session->flashdata('message'); ?>      
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="">
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9 form-input">
                            <input id="txt_name" name="txt_name" type="text" class="form-control" required="required" maxlength="50">
                        </div>
                    </div>                      

                    <div class="form-group my-form">
                        <label for="inputPassword3" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9 form-input">
                            <select id="ddl_status" name="ddl_status" class="js-states form-control" tabindex="-1" style=" width: 100%">
                                <option  value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>                           
                    
                    <div class="">
                        <div class="col-sm-offset-3 col-sm-9 mob-center">
                            <input type="submit" id="btnAdd" value="Add" class="btn btn-success" />
                            <!--<button class="btn btn-success">Cancel</button>-->
                        </div>
                    </div>
                </form>
            </div>

             

        </div>
    </div>

</div>
</div>

