<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Business Attributes List</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
     <div class="row">
        <div class="col-sm-5 mob-center">
            <h3>Add Business Attributes</h3>
        </div>

        <div class="col-sm-7 mob-center">
            <div class="pull-right">
                <a href="<?php echo site_url("add-business-attributes"); ?>"><button class="btn btn-success" type="button">Add Business Attributes</button></a>
            </div>
        </div>
    </div>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
			<div class="col-md-12">
               <div class="mailbox-content">
                <?= $this->session->flashdata('message'); ?> 
                <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                            <!--<th class="hidden-xs" width="4%"><input type="checkbox" class="check-mail-all"></th>-->
                            <th class="hidden-xs" width="5%">S.No</th>
                            <th>Dispaly Name</th>
                            <th>Module Name</th>
                            <th>Attribute For</th>
                            <th>Data Type</th>
                            <th>Is Required</th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody id="tbl_body">                   
                     <?php $i=0;
                     foreach($business_attributes_list as $row)
                     {
                        echo "<td class='hidden-xs'>". ($i + 1) ."</td>";
                        echo "<td>".$row["display_name"]."</td>";
                        echo "<td>".$row["module_name"]."</td>";

                        if($row["type"] == 2)
                        {
                            echo "<td>Salary</td>";
                        }
                        else
                        {
                            echo "<td>Employee</td>";
                        }

                        if($row["data_type_code"] == 'INT')
                        {
                            echo "<td>NUMBER</td>";
                        }
                        elseif($row["data_type_code"] == 'VARCHAR')
                        {
                            echo "<td>STRING</td>";
                        }
                        else
                        {
                            echo "<td style='text-transform: lowercase; text-transform:capitalize;'>".$row["data_type_code"]."</td>";
                        }

                        if($row["is_required"])
                        {
                            echo "<td><b>Yes</b></td>";
                        }
                        else
                        {
                            echo "<td>No</td>";
                        }
                        echo "<td>";                        
                        echo '<a href="'.site_url("edit-business-attributes/".$row["id"]).'"><span class="fa fa-edit"></span></a>';
                        //echo ' | <a href=#><span class="fa fa-trash-o"></span></a>';
                        echo "</td>";
                        echo "</tr>";
                $i++;
                     }
                     ?>  
                     
                    </tbody>
                   </table>                    
                </div>
            </div>
</div>
</div>

