<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Upload File</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Upload File</h3>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-white">        
            <div class="panel-body">
                <?php if(isset($performance_cycle_id)){ ?>
                <form class="form-horizontal" method="post" action="<?php echo site_url("upload-data/".$performance_cycle_id); ?>" enctype="multipart/form-data">
                <?php }else{ ?>
                 <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                <?php } ?>
                                        
                    <div class="form-group my-form">
                        <!--<label for="inputEmail3" class="col-sm-3 control-label">Choose File</label>-->
                        <div class="col-sm-11 form-input browse-wrap">  
                        <div class="title">Choose a csv file to upload</div>                          
                            <input type="file" name="myfile" id="myfile" class="form-control upload" title="Choose a file csv to upload" required accept="application/msexcel*">
                        </div>
                        <span class="upload-path"></span>
                    </div>                                       
                    
                    <div class="">
                        <div class="col-sm-offset-5 col-sm-7 mob-center">
                            <input type="submit" id="btnAdd" value="Upload" class="btn btn-success" />
                            <!--<button class="btn btn-success">Cancel</button>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
 <script type="text/javascript">
    var span = document.getElementsByClassName('upload-path');
    // Button
    var uploader = document.getElementsByName('myfile');
    // On change
    for( item in uploader ) {
      // Detect changes
      uploader[item].onchange = function() {
        // Echo filename in span
        span[0].innerHTML = this.files[0].name;
         if(this.files[0].type != "text/comma-separated-values")
        {
            span[0].innerHTML = "";
            $("#myfile").val("");
            alert("Choosed file size should csv only.")
        }
        else if(this.files[0].size > (1*1024*1000))
        {
            $("#myfile").val("");
            span[0].innerHTML = "";
            alert("Choosed file size should be less than 1MB.")
        }
      }
    }
</script>
