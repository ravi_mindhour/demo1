<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("manager/dashboard"); ?>">Dashboard</a></li>
        <li class="active">Performance Cycle</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Performance Cycle</h3>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
    <div class="col-md-12">
               <div class="mailbox-content">
                <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                            <!--<th class="hidden-xs" width="4%"><input type="checkbox" class="check-mail-all"></th>-->
                            <th class="hidden-xs" width="5%">S.No</th>
                            <th>Cycle Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Desciption</th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody id="tbl_body">                   
                     <?php $i=0;

                     if($performance_cycle_list)
                     {
                         foreach($performance_cycle_list as $row)
                         {
    						 $id = $row["id"];
                             $uploadid = $row["upload_id"];
                            echo "<tr><td class='hidden-xs'>". ($i + 1) ."</td>";
                            echo "<td>".$row["name"]."</td>";
                            if($row["start_date"] != '0000-00-00')
                            {
                                 echo "<td>".date("d/m/Y", strtotime($row["start_date"]))."</td>";
                            }
                            else
                            {
                                 echo "<td></td>";
                            }
                            if($row["end_date"] != '0000-00-00')
                            {
                                echo "<td>".date("d/m/Y", strtotime($row["end_date"]))."</td>";
                            }
                            else
                            {
                               echo "<td></td>"; 
                            }                         
                            
                            echo "<td>".$row["description"]."</td>";
                            if($row["status"]==7)
                            {
                                echo "<td><a href='".site_url("manager/view-increments-list/$id")."'>View Increments</a></td>";
                            }
                            else
                            {echo "<td></td>";}

                            echo "</tr>";
                            $i++;
                         }
                     }
                     ?>  
                     
                    </tbody>
                   </table>                    
                </div>
            </div>
</div>
</div>

<!-- Common popup to give a alert msg Start -->

<script type="text/javascript">
<?php if($this->session->flashdata("message")){?>
$("#common_popup_for_alert").html('<?php echo "".$this->session->flashdata("message").""; ?>');
    $.magnificPopup.open({
        items: {
            src: '#common_popup_for_alert'
        },
        type: 'inline'
    });
setTimeout(function(){ $('#common_popup_for_alert').magnificPopup('close');}, 3000);
<?php } ?>
</script>

<!--  Common popup to give a alert msg End -->