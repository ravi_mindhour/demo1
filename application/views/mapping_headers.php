<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Mapping Headers</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Mapping Headers</h3>
</div>
</div>
<div id="loading" style="position:absolute;width:100%;height:100%; top:0; z-index:9999; background:#000;display:none; opacity:.5;">

<img src="<?php echo base_url("assets/loading.gif"); ?>" style="width:20%; margin-left:40%; margin-top:10%;" />

</div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
       <!-- <select id="selTemplate">
        </select>-->
        
            <div class="">
                <div class="mailbox-content">
                	<form class="form-horizontal" id="frm_mapping" method="post">
                    <input type="hidden" name="hf_upload_id" value="<?php echo $upload_id; ?>" />
                        <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    <!--<th class="hidden-xs" width="4%"><input type="checkbox" class="check-mail-all"></th>-->
                                    <th class="hidden-xs" width="5%">S.No</th>
                                    <th>Header Name</th>
                                    <th>Enter Display Name</th>
                                    
                                    
                                    <th> Mapping </th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <script>$ids = [];</script>
                             <?php $i=0; $ids;
                        
                            $ddl_map_opt_list = "";
                             foreach($business_attribute_list as $row)
                             {
                                 $ddl_map_opt_list .= "<option value='".$row["id"]."".CV_CONCATENATE_SYNTAX."".$row["module_name"]."'>".$row["display_name"]."</option>";
                                 echo "<input name='hf_attribute_ids[]' value='".$row["id"]."' type='hidden' />"; 
                             }
                        
                             foreach($header_list as $row)
                             {
                                echo "<td class='hidden-xs'>". ($i + 1) ."</td>";
                                echo "<td>".$row."</td>";
                                echo "<td><input name='txt_head_name[]' value='".$row."' type='text' id='txt".$i."' />"; 
                                echo "<td><select name='ddl_mapping[]' onchange='selChange(this);' class='sel_mapp' id='sel_data".$i."'>";
                                echo "<option value='N/A'> Select Mapping </option>";
                                echo $ddl_map_opt_list;						
                                echo "</select></td>";
                                echo "</tr>";
                                $i++;
                             }
                             ?>  
                            </tbody>
                           </table>
                           
                        <div class="m-t-lg mob-center">
                           <!--<input style="visibility:hidden;" type="text" id="txtTemplate" placeholder="Enter Template Name" />
                           <label> Save as template </label>
                            <input type="checkBox" class="btn  "  id="chkTemplate" onclick="showTemplateText(this);" />-->
                             <input type="button" class="btn btn-twitter m-b-sm add-btn" value="Save" id="btnSave" />
                                                             </div>                    
                	</form>                    
                 </div>
            </div>
        </div>
    </div>
</div><!-- Row -->
</div>

<script>
function selChange(str)
{	
	var prevValue = $(str).data('previous');
	$('select').not(str).find('option[value="'+prevValue+'"]').show();    
	var value = $(str).val();
	$(str).data('previous',value); 
	$('select').not(str).find('option[value="'+value+'"]').hide();  
}

$(function()
{						
	$('#btnSave').click(function()
	{

		var i = 0;var x = 0;var y = 0;var z = 0;var increment_applied_on = 0;
		$("select[name='ddl_mapping[]']").each( function (key, v)
		{
			if($(this).val() != 'N/A')
			{
				i = 1;
				var splt_arr = ($(this).val()).split('<?php echo CV_CONCATENATE_SYNTAX; ?>')
				if(splt_arr[1] == '<?php echo CV_EMAIL_MODULE_NAME; ?>')
				{
					x = 1;								
				}	
				if(splt_arr[1] == '<?php echo CV_SALARY_ELEMENT; ?>')
				{
					y = 1;								
				}
				if(splt_arr[1] == '<?php echo CV_MARKET_SALARY_ELEMENT; ?>')
				{
					z = 1;								
				}
				if(splt_arr[1] == '<?php echo CV_INCREMENT_APPLIED_ON; ?>')
				{
					increment_applied_on = 1;								
				}			
			}			
		});
		
		if(i == 0)
		{
			alert("You must map the headers.");
			return false;	
		}
		if(x == 0)
		{
			alert("You must map the email header.");
			return false;	
		}
		if(y == 0)
		{
			alert("You must map the salary header.");
			return false;	
		}
		if(z == 0)
		{
			alert("You must map the market salary header.");
			return false;	
		}
		if(increment_applied_on == 0)
		{
			alert("You must map the increment applied on header.");
			return false;	
		}

		$("#loading").css('display','block');
		<?php /*?>if($flag_check == 1)
		{
			if($("#txtTemplate").val()=="" || $("#txtTemplate").val() == null)
			{
				alert("Please Enter Template Name");
				return false;
			}
		}<?php */?>
		<?php /*?>$str_error = "";
			
		str = "";
		var sel = "";
		var sel_type = "";
		var sel_mapp = "";
		//var template_name = $("#txtTemplate").val();
		
		$('#tbl_body input[type=text]').each(function ()
		{
			str+=$(this).val() + ",";		
		});
				
		$('#tbl_body .sel').each(function()
		{		
			sel += $(this).val() + ",";		
		});
		
		$('#tbl_body .sel_data').each(function()
		{
			sel_type += $(this).val() + ",";		
		});
		
		$('#tbl_body .sel_mapp').each(function()
		{
			sel_mapp += $(this).val() + ",";		
		});
		
		//To remove the extra $ at end  
		if(str != "") 
		{
			str = str.substring(0,str.length-1);
		}
		if(sel != "")
		{
			sel = sel.substring(0,sel.length - 1);
		}
		
		if(sel_type != "")
		{
			sel_type = sel_type.substring(0,sel_type.length - 1);
		}
		if(sel_mapp != "")
		{
			sel_mapp = sel_mapp.substring(0,sel_mapp.length - 1);
		}<?php */?>


		var form=$("#frm_mapping");
		$.ajax({
			type:"POST",
			url:"<?php echo site_url("upload/insert_headers");?>",
			data:form.serialize(),
		
			success: function(response)
	        {
	        	$("#loading").css('display','none');
	    		if(response)
	            {
	                window.location.href= "<?php echo site_url("performance-cycle");?>";
	    		} 
	            else
	            {
	                //show error
	    		}
			}
		});

		<?php /*?>var post_data = {ids: $ids, newValue: str, upload_id:<?php echo $upload_id; ?>,module:sel,dataType:sel_type,map:sel_mapp};
		
		 $.post("<?php echo site_url("upload/insert_headers");?>",post_data, function(data,status)
		 {
			// console.log(data);
			////$cData = jQuery.parseJSON(data);
			////$error = $cData.error;
			////$i = 0;			
			//alert($str_error);
			//$.cookie('error', null);
			////$.cookie("error",data);
			//window.location.href= "<?php echo site_url("show-uploaded-data/".$upload_id);?>";
			//window.location.href= "<?php echo site_url("performance-cycle");?>";
		});	<?php */?>	
	});		
});
</script>