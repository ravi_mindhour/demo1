<?php if(!$rule_dtls){?>
    <form id="frm_rule_step1" method="post" onsubmit="return set_rules(this);" action="<?php echo site_url("rules/set_rules/1"); ?>">
		<input type="hidden" value="<?php echo $performance_cycle_id; ?>" name="hf_performance_cycle_id" />
		<div class="row">
			<div class="col-sm-6">
				<?php /*?><div class="form-group">
                	<div class="row">
                    	<div class="col-sm-6">
                       	 	<label class="control-label">Salary Increase for Fiscal year</label>
                        </div>
                        <div class="col-sm-6">
                            <select class="form-control" name="ddl_fiscal_year" required>
                                <option value="">Select</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                            </select>
                        </div>
					</div>
                </div>
				<div class="form-group">
					<div class="row">
                    	<div class="col-sm-6">
                    		<label for="inputPassword" class="control-label">Increase to be applied on salary element</label>            
						</div>
                    	<div class="col-sm-6">
							<select class="form-control" name="ddl_salary_element" required >
								<option value="">Select</option>
								<?php foreach($salary_elements_list as $row)
								{
								echo "<option value=".$row["id"].">".$row["display_name"]."</option>";
								}?>                                           
							</select>
						</div>            
					</div>
                </div><?php */?>
				<div class="form-group">
                	<div class="row">
                    	<div class="col-sm-6">
							<label for="inputPassword" class="control-label">Include Inactive</label>            
                       	</div>
                        <div class="col-sm-6">
                            <select class="form-control" name="ddl_include_inactive" required>
                                <option value="yes">Yes</option>
                                <option value="no" selected="selected">No</option>
                            </select>
                        </div> 
                  	</div>           
				</div>
			</div>
		</div>
		
        <div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<div class="row">
                   		<div class="col-sm-6">
                            <label class="control-label">Performnace Based Hikes</label>
						</div>
                    	<div class="col-sm-6">
							<select class="form-control" id="ddl_performnace_based_hikes" name="ddl_performnace_based_hikes" required onchange="manage_performnace_based_hikes(this.value);">
								<option value="">Select</option>
								<option value="yes">Yes</option>
								<option value="no">No</option>
							</select>
						</div>
					</div>
                </div>
				
                <div id="perfom-y">
					<div class="row">
						<div class="col-sm-12" style="display:none;" id="dv_rating_list_yes"></div>
					</div>        
				</div>
				
                <div id="perfom-N">
					<div class="row">
						<div class="col-sm-12" style="display:none;" id="dv_rating_list_no">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
                                    	<label for="inputEmail3" class="control-label">Rating</label>
                                   	</div>
                              		<div class="col-sm-6">
										<input type="text" class="form-control" name="txt_rating1" id="txt_rating1" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);">
									</div>
								</div>
                            </div>
						</div>
					</div>
				</div>
                
			</div>  
		</div>

         <div class="row">          
			<div class="col-sm-6">
				<div class="form-group">
					<div class="row">
                   		<div class="col-sm-6">
                            <label class="control-label">Comparative ratio to be applied after performance based merit increase</label>
						</div>
                    	<div class="col-sm-6">
							<select class="form-control" id="ddl_comparative_ratio" name="ddl_comparative_ratio" required onchange="manage_comparative_ratio(this.value);">
								<option value="">Select</option>
								<option value="yes">Yes</option>
								<option value="no">No</option>
							</select>
						</div>
					</div>
                </div>
				
                <div id="perfom-y">
					<div class="row">
						<div class="col-sm-12" id="dv_apply_comparative_ratio_yes" style="display:none;"></div>
					</div>        
				</div>

                <div id="perfom-N">
					<div class="row">
						<div class="col-sm-12" id="dv_apply_comparative_ratio_no" style="display:none;">
							<div class="form-group">
								<div class="row">
                            		<div class="col-sm-6">
                                        <label for="inputEmail3" class="control-label">Comparative Ratio</label>
									</div>
                                	<div class="col-sm-6">	
										<select class="form-control" name="ddl_comparative_ratio1" id="ddl_comparative_ratio1">
										</select>
									</div>
								</div>
                            </div>
						</div>
					</div>
				</div>

            </div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<?php /*?><div class="form-group">
					<div class="row">
                    	<div class="col-sm-6">
                    		<label class="control-label">Comparative ratio ranges (CRR) Elements</label>
						</div>
                    	<div class="col-sm-6">
							<select class="form-control" name="ddl_comparative_ratio_salary_element" required >
								<option value="">Select</option>
								<?php foreach($salary_elements_list as $row)
								{
								echo "<option value=".$row["id"].">".$row["display_name"]."</option>";
								}?>                                           
							</select>
						</div>
					</div>
                </div><?php */?>        
				<div class="form-group">
					<div class="row">
                    	<div class="col-sm-6">
                        	<label class="control-label">Comparative ratio ranges (CRR)</label>
						</div>
                   		<div class="col-sm-6">
                        	<div class="row">
                            	<div class="col-sm-12">
                                	<div class="form-group">
										<label class="control-label">Range</label>
                                    </div>
                               	</div>
                                <div class="col-sm-12">
                                    <div class="form-group rv1" id="dv_new_ratio_range_row">
                                    	<div class="row">
                                            <div class="col-sm-8">
                                                <input type="text" name="txt_crr[]" class="form-control" id="txt_crr" placeholder="Range" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);">
                                            </div>
                                            <div class="col-sm-4" id="dv_btn_dlt"></div>
                                      	</div>
                                    </div>
                                   	<div class="row" id="dv_btn_add_row">
                                        <div class="col-sm-5 col-sm-offset-7">
                                            <button type="button" class="btn btn-primary" onclick="addAnotherRow();">ADD CRR</button>
                                        </div>   
                                    </div>
                              	</div>
                           	</div>
						</div>
					</div>
					
				</div>
			</div>
		</div> 

        <div class="row">  
			<div class="col-sm-6">    
				<div class="form-group">
					<div class="row">
                    	<div class="col-sm-6">
                            <label class="control-label">Prorated Increase</label>
						</div>
                    	<div class="col-sm-6">
							<select class="form-control" name="ddl_prorated_increase" required>
								<option value="yes">Yes</option>
								<option value="no" selected="selected">No</option>
							</select>
						</div>
					</div>
                </div>    
			</div>   
		</div>
        <div class="row">
            <div class="col-sm-6">
				<div class="form-group">
					<div class="row">
                    	<div class="col-sm-6">
                    		<label class="control-label">Manager's discretionary increase </label>
						</div>
                    	<div class="col-sm-3">
							<span style="font-weight:bold; font-size:20px; margin-left:50px;">-</span> <br>
							<input type="text" name="txt_manager_discretionary_decrease" class="form-control" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);" />
						</div>
						<div class="col-sm-3">
							<span style="font-weight:bold; font-size:20px; margin-left:50px;">+</span><br>
							<input type="text" name="txt_manager_discretionary_increase" class="form-control" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"/>
						</div>
					</div>
                </div>
			</div>   
		</div> 
		
        <div class="row" id="dv_frm_1_btn">
			<div class="col-sm-offset-5 col-sm-2 mob-center">
				<input type="submit" id="btnAdd_1st" value="Submit" class="btn btn-success" />
			</div>
		</div>

    </form>

<?php }elseif($rule_dtls["status"]==1){ ?>

	<form id="frm_rule_step2" method="post" onsubmit="return set_rules(this);" action="<?php echo site_url("rules/set_rules/2"); ?>">
		<input type="hidden" value="<?php echo $performance_cycle_id; ?>" name="hf_performance_cycle_id" />
		<input type="hidden" value="<?php echo $rule_dtls["id"]; ?>" name="hf_rule_id" />
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-bordered">
					<thead>
			  			<tr>
							<th>Ratings</th>
							<th>Market Benchmark</th>
							<?php $i=0; $crr_arr = json_decode($rule_dtls["comparative_ratio_range"], true);
								foreach($crr_arr as $row)
					 			{ 
							?>
							<th> 
								<?php 
                                    if($i==0)
                                    { 
                                        echo "<".$row["max"];
                                    }
                                    elseif(($i+1)==count($crr_arr))
                                    {
                                    echo " > ".$row["max"];
                                    }
                                    else
                                    {
                                        echo $row["min"]." < ".$row["max"];
                                	}
								?>CR 
                            </th>
							<?php 
									$i++; 
								} 
							?>
							<th style="background-color: #22baa0; color: #fff">Max Hike</th>
							<th style="background-color: #22baa0; color: #fff">Recently promoted max salary increase</th>
							<input type="hidden" value="<?php echo count($crr_arr); ?>" name="hf_crr_counts" />
						</tr>
					</thead>
					<tbody>
			 		<?php 
			 			$i=0; $crr_arr1 = json_decode($rule_dtls["comparative_ratio_calculations"],true);
			 			//echo "<pre>";print_r($rule_dtls);die;
				  		foreach($crr_arr1 as $key => $val)
						{ 
					?>
			  		<tr>
						<td>
						<?php $rating_name_arr =explode(CV_CONCATENATE_SYNTAX, $key); 
							if($rule_dtls["comparative_ratio"] == 'yes')
							{
								echo $rating_name_arr[1];
							}
							else
							{
								echo $rating_name_arr[0];
							}
						?>
							
						</td>
						<td><?php $market_salalry_arr =explode(CV_CONCATENATE_SYNTAX, $val); echo $market_salalry_arr[1]; ?></td>
						<?php 
							$i = 0;
							foreach($crr_arr as $row)
					 		{ 
						?>                		
						<td><input type="text" class="form-control" name="txt_range_val<?php echo $i; ?>[]" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"></td>      
						<?php 
							$i++; 
							} 
						?>
						<td><input type="text" class="form-control" name="txt_max_hike[]" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"></td> 
						<td><input type="text" class="form-control" name="txt_recently_promoted_max_salary_increase[]" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"></td> 
					</tr>
			 		<?php 
						} 
					?>
				</tbody>
		  	</table>
			</div>
		</div>        
		
        <div class="row">
			<div class="col-sm-6">            
				<div class="form-group">
                	<div class="row">
                    	<div class="col-sm-6">
							<label class="control-label">Standard Promotion increase</label>
                        </div>
						<div class="col-sm-6">
							<input type="text" name="txt_standard_promotion_increase" class="form-control" required="required" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"/>
						</div>
					</div>
                </div>
				<div class="form-group">
					<div class="row">
                    	<div class="col-sm-6">
                			<label class="control-label">Oveall budget allocated for salary increase for the team</label>
						</div>
                		<div class="col-sm-6">
							<select class="form-control" name="ddl_overall_budget" required onchange="show_hide_budget_dv(this.value);">
								<option value="">Select</option>
								<option value="No limit">No limit</option>
								<option value="Automated locked">Automated locked</option>
								<option value="Automated but x% can exceed">Automated but x% can exceed</option>
								<option value="Manual">Manual</option>
							</select>
							<br />
				 			<input type="text" class="form-control" name="txt_budget_amt" id="txt_budget_amt" placeholder="Budget Amount" style="display:none;" maxlength="10" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"> 
                            <br />
				  			<input type="text" class="form-control" name="txt_budget_percent" id="txt_budget_percent" placeholder="Budget %" style="display:none;" maxlength="5" onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);"> 	
                 		</div>
					</div>
                </div>
			</div>            
		</div>
		<div class="row">
	   		<div class="col-sm-2 col-sm-offset-10">
				<div class="submit-btn">
	  				<button type="submit" id="btnAdd_2nd" class="btn btn-success">SUBMIT</button>
	  			</div>
			</div>
		</div>
	</form>
<?php } ?>


            