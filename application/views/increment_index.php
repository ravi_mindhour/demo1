<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Increment Calculation</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Increment Calculation</h3>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
    <div class="col-md-12">
        <div class="panel panel-white">
        <?= $this->session->flashdata('message'); ?>      
            <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group ">
                    <label for="inputEmail3" class="col-sm-3 control-label">Cycle Name</label>
                    <div class="col-sm-2 form-input">
                        <select class='form-control' name="ddl_performance_cycle" id="ddl_performance_cycle" required onchange="get_staff_list(this.value);">
                            <?php 
                            foreach($performance_cycle_list as $row)
                            {									
                                echo "<option value=".$row["id"].">".$row["name"]."</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div id="dv_partial_page_data">
                </div>  
            </div>                 
            </div>
        </div>
    </div>
    
</div>
</div>

<script type="text/javascript">
get_staff_list($("#ddl_performance_cycle").val());
function get_staff_list(val)
{
	if(val)
	{
		 $.post("<?php echo site_url("increments/get_staff_list");?>",{pid:val}, function(data)
         {
           if(data)
           {
            $("#dv_partial_page_data").html(data);
           }
        }); 
	}
	else
	{
		$("#dv_partial_page_data").html("");
	}
}
</script>