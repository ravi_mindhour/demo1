<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Performance Cycle</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Performance Cycle</h3>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-white">
       
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="<?php echo site_url("performance-cycle"); ?>">
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Cycle Name</label>
                        <div class="col-sm-9 form-input">
                            <input id="txt_performance_cycle_name" name="txt_performance_cycle_name" type="text" class="form-control" required="required" maxlength="100">
                        </div>
                    </div>                   
                    
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Start Date</label>
                        <div class="col-sm-9 form-input">
                            <input id="txt_start_dt" name="txt_start_dt" type="text" class="form-control" required="required" maxlength="10">
                        </div>
                    </div>
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">End Date</label>
                        <div class="col-sm-9 form-input">
                            <input id="txt_end_dt" name="txt_end_dt" type="text" class="form-control" required="required" maxlength="10">
                        </div>
                    </div> 
                    
                    <div class="form-group my-form">
                        <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9 form-input">
                            <input id="txt_description" name="txt_description" type="text" class="form-control">
                        </div>
                    </div>                               
                    
                    <div class="">
                        <div class="col-sm-offset-3 col-sm-9 mob-center">
                            <input type="submit" id="btnAdd" value="Add" class="btn btn-success" />
                            <!--<button class="btn btn-success">Cancel</button>-->
                        </div>
                    </div>
                </form>
            </div>

             

        </div>
    </div><div class="col-md-12">
               <div class="mailbox-content">
                <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                            <!--<th class="hidden-xs" width="4%"><input type="checkbox" class="check-mail-all"></th>-->
                            <th class="hidden-xs" width="5%">S.No</th>
                            <th>Cycle Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Desciption</th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody id="tbl_body">                   
                     <?php $i=0;

                    if($approvel_request_list)
                    {
                        foreach($approvel_request_list as $row)
                        {
                            $upload_id = $row["upload_id"];
                            echo "<tr><td class='hidden-xs'>". ($i + 1) ."</td>";
                            echo "<td>".$row["name"]."</td>";
                           if($row["start_date"] != '0000-00-00')
                            {
                                 echo "<td>".date("d/m/Y", strtotime($row["start_date"]))."</td>";
                            }
                            else
                            {
                                 echo "<td></td>";
                            }
                            if($row["end_date"] != '0000-00-00')
                            {
                                echo "<td>".date("d/m/Y", strtotime($row["end_date"]))."</td>";
                            }
                            else
                            {
                               echo "<td></td>"; 
                            }        
                            echo "<td>".$row["description"]."</td>";
                            echo "<td><a href='".site_url("show-uploaded-data/$upload_id")."'>Sheet Approval Details</a></td>";
                            echo "</tr>";
                            $i++;
                        }
                    }

                    if($rules_approvel_request_list)
                    {
                        foreach($rules_approvel_request_list as $row)
                        {
                            $upload_id = $row["upload_id"];
                            echo "<tr><td class='hidden-xs'>". ($i + 1) ."</td>";
                            echo "<td>".$row["name"]."</td>";
                            if($row["start_date"] != '0000-00-00')
                            {
                                 echo "<td>".date("d/m/Y", strtotime($row["start_date"]))."</td>";
                            }
                            else
                            {
                                 echo "<td></td>";
                            }
                            if($row["end_date"] != '0000-00-00')
                            {
                                echo "<td>".date("d/m/Y", strtotime($row["end_date"]))."</td>";
                            }
                            else
                            {
                               echo "<td></td>"; 
                            }        
                            echo "<td>".$row["description"]."</td>";
                            echo "<td><a href='".site_url("view-rule/".$row["id"]."")."'>Rule Approval Details</a></td>";
                            echo "</tr>";
                            $i++;
                         }
                    }

                     if($performance_cycle_list)
                     {
                         foreach($performance_cycle_list as $row)
                         {
    						 $id = $row["id"];
                             $uploadid = $row["upload_id"];
                            echo "<tr><td class='hidden-xs'>". ($i + 1) ."</td>";
                            echo "<td>".$row["name"]."</td>";
                            if($row["start_date"] != '0000-00-00')
                            {
                                 echo "<td>".date("d/m/Y", strtotime($row["start_date"]))."</td>";
                            }
                            else
                            {
                                 echo "<td></td>";
                            }
                            if($row["end_date"] != '0000-00-00')
                            {
                                echo "<td>".date("d/m/Y", strtotime($row["end_date"]))."</td>";
                            }
                            else
                            {
                               echo "<td></td>"; 
                            }                         
                            
                            echo "<td>".$row["description"]."</td>";
                            if($row["status"]==1)
                            {
                                echo "<td><a href='".site_url("upload-data/$id")."'>Upload File</a></td>";
                               
                            }
                            elseif($row["status"]==2)
                            {
                                echo "<td><a href='".site_url("mapping-head/$id")."'>Map Headers</a></td>";
                            }
                            elseif($row["status"]==3)
                            {                                
                                 echo "<td><a href='".site_url("show-uploaded-data/$uploadid")."'>View Sheet Details</a> | Sheet Approval Pending</td>";
                            }
                            elseif($row["status"]==4)
                            {                             
                                echo "<td><a href='".site_url("show-uploaded-data/$uploadid")."'>View Sheet Details</a> | <a href='".site_url("create-rules/$id")."'>Create Rules</a></td>";
                            }
                            elseif($row["status"]==5)
                            {
                                echo "<td><a href='".site_url("show-uploaded-data/$uploadid")."'>View Sheet Details</a> | <a href='".site_url("view-rule/".$row["id"]."")."'>View Rule</a> | <a href='".site_url("view-rule-budget/$uploadid")."'>Send Rule For Approval</a></td>";
                            }
                            elseif($row["status"]==6)
                            {
                                echo "<td><a href='".site_url("show-uploaded-data/$uploadid")."'>View Sheet Details</a> | <a href='".site_url("view-rule/".$row["id"]."")."'>View Rule</a> | Rule Approval Pending</td>";
                            }
                            elseif($row["status"]==7)
                            {
                                echo "<td><a href='".site_url("show-uploaded-data/$uploadid")."'>View Sheet Details</a> | <a href='".site_url("view-rule/".$row["id"]."")."'>View Rule</a> | <a href='".site_url("view-increments/$id")."'>View Increments</a></td>";
                            }
                            else
                            {echo "<td></td>";}

                            echo "</tr>";
                            $i++;
                         }
                     }
                     ?>  
                     
                    </tbody>
                   </table>                    
                </div>
            </div>
</div>
</div>

<script> 

$(document).ready(function() {
    //$.noConflict();
    $( "#txt_start_dt,#txt_end_dt" ).datepicker({ 
    dateFormat: 'dd/mm/yy',
    changeMonth : true,
    changeYear : true,
       // yearRange: "1995:new Date().getFullYear()",
     });      
 });
</script>

<!-- Common popup to give a alert msg Start -->

<script type="text/javascript">
<?php if($this->session->flashdata("message")){?>
$("#common_popup_for_alert").html('<?php echo "".$this->session->flashdata("message").""; ?>');
    $.magnificPopup.open({
        items: {
            src: '#common_popup_for_alert'
        },
        type: 'inline'
    });
setTimeout(function(){ $('#common_popup_for_alert').magnificPopup('close');}, 3000);
<?php } ?>
</script>

<!--  Common popup to give a alert msg End -->