<div id="main-wrapper" class="container">

<div class="row mb20">
    <div class="col-lg-5 col-md-6">
        <div class="panel info-box panel-white">
            <div class="panel-body">
                <div class="info-box-stats">
                    <p class="counter"><?php echo round($total_max_budget,2); ?>/-</p>
                    <span class="info-box-title">Total Max Budget</span>
                </div>
                <div class="info-box-icon">
                    <i class="fa fa-bank"></i>
                </div>
                <div class="info-box-progress">
                    <div class="progress progress-xs progress-squared bs-n">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5 col-md-6">
        <div class="panel info-box panel-white">
            <div class="panel-body">
                <div class="info-box-stats">
                    <p class="counter"><?php echo round($all_emp_total_salary,2); ?>/-</p>
                    <span class="info-box-title">All Employee Total Salary</span>
                </div>
                <div class="info-box-icon">
                    <i class="fa fa-users"></i>
                </div>
                <div class="info-box-progress">
                    <div class="progress progress-xs progress-squared bs-n">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    <div class="col-lg-6">
      <div class="m-t-lg mob-center">
        <a href="<?php echo site_url("send-rule-for-approval/".$upload_id); ?>">
          <input type="button" class="btn btn-twitter m-b-sm add-btn" value="Send For Approval" id="btnSave" />
        </a>
      </div>
   </div>
               

</div>

</div>