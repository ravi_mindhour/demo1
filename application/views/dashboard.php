<div id="main-wrapper" class="container">

<div class="row mb20">
    <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
            <div class="panel-body">
                <div class="info-box-stats">
                    <p class="counter"><?php echo $statics["performance_cycle"]; ?></p>
                    <span class="info-box-title">No. Of Performance Cycle</span>
                </div>
                <div class="info-box-icon">
                    <i class="fa fa-bank"></i>
                </div>
                <div class="info-box-progress">
                    <div class="progress progress-xs progress-squared bs-n">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
            <div class="panel-body">
                <div class="info-box-stats">
                    <p class="counter"><?php echo $statics["hr_admin_user"]; ?></p>
                    <span class="info-box-title">No. Of HR Admin Users</span>
                </div>
                <div class="info-box-icon">
                    <i class="fa fa-users"></i>
                </div>
                <div class="info-box-progress">
                    <div class="progress progress-xs progress-squared bs-n">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
            <div class="panel-body">
                <div class="info-box-stats">
                    <p class="counter"><?php echo $statics["hr_user"]; ?></p>
                    <span class="info-box-title">No. Of HR Users</span>
                </div>
                <div class="info-box-icon">
                    <i class="icon-envelope"></i>
                </div>
                <div class="info-box-progress">
                    <div class="progress progress-xs progress-squared bs-n">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
            <div class="panel-body">
                <div class="info-box-stats">
                    <p class="counter"><?php echo $statics["employee"]; ?></p>
                    <span class="info-box-title">No. Of Users</span>
                </div>
                <div class="info-box-icon">
                    <i class="fa fa-users"></i>
                </div>
                <div class="info-box-progress">
                    <div class="progress progress-xs progress-squared bs-n">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row mb20">
                    <!--<div class="col-lg-6 col-md-12 hide-mob">
                        <div class="panel panel-white">
                                    <div class="visitors-chart">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Sales Chart</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div id="flotchart1"></div>
                                            <div id="flotchart2" style="display:none;"></div>
                                        </div>
                                    </div>
                        </div>
                    </div> -->
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-white datatable" style="height:100%;">
                            <div class="panel-heading">
                                <h4 class="panel-title">My Information</h4>
                                <div class="panel-control">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>                                    </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive project-stats">  
                                   <table class="table table-d">
                                       <tbody>
                                           <tr>
                                               <td width="45%">Name</td>
                                               <td width="55%">System Administrator</td>
                                           </tr>
                                           <tr>
                                               <td>User Name</td>
                                               <td>Admin</td>
                                           </tr>
                                           <tr>
                                               <td>Employee Type</td>
                                               <td>Regular Employee</td>
                                           </tr>
                                           <tr>
                                               <td>Employee Category</td>
                                               <td>Management</td>
                                           </tr>
                                           <tr>
                                               <td>Department</td>
                                               <td>Administration</td>
                                           </tr>
                                           <tr>
                                               <td>Station</td>
                                               <td>Head Office Name</td>
                                           </tr>
                                           <tr>
                                               <td>Last Login on</td>
                                               <td>March 14th 2014 at 11:32am</td>
                                           </tr>
                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-white datatable" style="height: 100%;">
                            <div class="panel-heading">
                                <h4 class="panel-title">Alerts</h4>
                                <div class="panel-control">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>                                    </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive project-stats">  
                                   <table class="table table-d">
                                       
                                       <tbody>
                                           <tr>
                                               <td><span class="fa fa-envelope"></span> Messages</td>
                                               <td>System Adminstrator</td>
                                           </tr>
                                           <tr>
                                               <td><span class="fa fa-user"></span> Requests</td>
                                               <td>Admin</td>
                                           </tr>
                                           <tr>
                                               <td><span class="fa fa-bank"></span> Company Name Update</td>
                                               <td>Regular Employees</td>
                                           </tr>
                                           <tr>
                                               <td><span class="fa fa-birthday-cake"></span> Birth Days </td>
                                               <td>Management</td>
                                           </tr>
                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
        <div class="panel panel-white datatable" style="height:100%;">
            <div class="panel-heading">
                <h4 class="panel-title">Reporting</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>                                    </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive project-stats">  
                   <table class="table table-d">
                       <tbody>
                           <tr>
                               <td>Report To   </td>
                               <td>John Smith</td>
                           </tr>
                           <tr>
                               <td>Reporting To Me	</td>
                               <td>Admin</td>
                           </tr>
                       </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
                </div>


<div class="row">  
  
    <div class="col-md-6">
        <div class="panel panel-white datatable">
            <div class="panel-heading">
                <h3 class="panel-title">Statistics</h3>
            </div>
            <div class="panel-body">
                <div>
                    <canvas id="chart1"></canvas>
                    <canvas id="chart2" height="150" style="display:none;"></canvas>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6 col-md-6">
        <div class="panel panel-white datatable" style="height: 100%;">
            <div class="panel-heading">
                <h4 class="panel-title">Employees Progress</h4>
            </div>
            <div class="panel-body">
                <div>
                   <canvas id="chart3"></canvas>
                   <canvas id="chart4" style="display:none;"></canvas>
                   <canvas id="chart5" style="display:none;"></canvas>
                   <canvas id="chart6" style="display:none;"></canvas>
                   
                </div>
            </div>
        </div>
    </div>
    
</div>
</div>