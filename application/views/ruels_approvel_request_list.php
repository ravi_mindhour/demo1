<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Rule Approval Request</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Rule Approval Request</h3>
</div>
</div>

<div id="main-wrapper" class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<div class="row mb20">
			<div class="col-md-12">
               <div class="mailbox-content">
                <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                            <!--<th class="hidden-xs" width="4%"><input type="checkbox" class="check-mail-all"></th>-->
                            <th class="hidden-xs" width="5%">S.No</th>
                            <th>Performance Cycle Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Request Date</th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody id="tbl_body">                   
                     <?php $i=0;
                     foreach($rules_approvel_request_list as $row)
                     {
						 $upload_id = $row["upload_id"];
                        echo "<td class='hidden-xs'>". ($i + 1) ."</td>";
                        echo "<td>".$row["name"]."</td>";
                        echo "<td>".$row["start_date"]."</td>";
                        echo "<td>".$row["end_date"]."</td>";
                        echo "<td>".date("Y-m-d", strtotime($row["request_date"]))."</td>";
                        echo "<td><a href='#'>View Details</a> | <a href='#'>Approve</a></td>";
                echo "</tr>";
                $i++;
                     }
                     ?>  
                     
                    </tbody>
                   </table>                    
                </div>
            </div>
</div>
</div>

<script> 

$(document).ready(function() {
    //$.noConflict();
    $( "#txt_start_dt,#txt_end_dt" ).datepicker({ 
    dateFormat: 'yy/mm/dd',
    changeMonth : true,
    changeYear : true,
       // yearRange: "1995:new Date().getFullYear()",
     });      
 });
</script>