<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
        <li class="active">Mapping Headers</li>
    </ol>
</div>
<div class="page-title">
<div class="container">
    <h3>Mapping Headers</h3>
</div>
</div>
<div id="loading" style="position:absolute;width:100%;height:100%; top:0; z-index:9999; background:#000;display:none; opacity:.5;">

<img src="<?php echo base_url("assets/loading.gif"); ?>" style="width:20%; margin-left:40%; margin-top:10%;" />

</div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
       <!-- <select id="selTemplate">
        </select>-->
        
            <div class="">
                <div class="mailbox-content">
                	<form class="form-horizontal" id="frm_mapping" method="post"  onsubmit="return check_validat();" action="">
                    <input type="hidden" name="hf_upload_id" value="<?php echo $upload_id; ?>" />
                        <table id="example" class="table border" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    <th class="hidden-xs" width="5%">S.No</th>
                                    <th>Header Name</th>
                                    <th>Enter Display Name</th>                                    
                                    <th> Mapping </th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body">
                             <?php $i=0;
                        
                            $ddl_map_opt_list = "";
                             foreach($business_attribute_list as $row)
                             {
                                 $ddl_map_opt_list .= "<option value='".$row["id"]."".CV_CONCATENATE_SYNTAX."".$row["module_name"]."'>".$row["display_name"]."</option>";
                                 echo "<input name='hf_attribute_ids[]' value='".$row["id"]."' type='hidden' />"; 
                             }
                        
                             foreach($header_list as $row)
                             {
                                echo "<td class='hidden-xs'>". ($i + 1) ."</td>";
                                echo "<td>".$row."</td>";
                                echo "<td><input name='txt_head_name[]' value='".$row."' type='text' id='txt".$i."' />"; 
                                echo "<td><select name='ddl_mapping[]' onchange='selChange(this);' class='sel_mapp' id='sel_data".$i."'>";
                                echo "<option value=''> Select Mapping </option>";
                                echo $ddl_map_opt_list;						
                                echo "</select></td>";
                                echo "</tr>";
                                $i++;
                             }
                             ?>  
                            </tbody>
                           </table>
                           
                        <div class="m-t-lg mob-center">
                            <input type="submit" class="btn btn-twitter m-b-sm add-btn" value="Save" id="btnSave" />
                        </div>                    
                	</form>                    
                 </div>
            </div>
        </div>
    </div>
</div><!-- Row -->
</div>

<script>
function selChange(str)
{	
	var prevValue = $(str).data('previous');
	$('select').not(str).find('option[value="'+prevValue+'"]').show();    
	var value = $(str).val();
	$(str).data('previous',value); 
	$('select').not(str).find('option[value="'+value+'"]').hide();  
}

function check_validat()
{	
	var i = 0;var x = 0;var y = 0;var z = 0;
	$("select[name='ddl_mapping[]']").each( function (key, v)
	{
		if($(this).val() != '')
		{
            i = 1;
			var splt_arr = ($(this).val()).split('<?php echo CV_CONCATENATE_SYNTAX; ?>')
			if(splt_arr[1] == '<?php echo CV_EMPLOYEE_NAME; ?>')
			{
				x = 1;								
			}	
            if(splt_arr[1] == '<?php echo CV_EMAIL_MODULE_NAME; ?>')
            {
                y = 1;                              
            }
            if(splt_arr[1] == '<?php echo CV_DESIGNATION; ?>')
            {
                z = 1;                              
            }   		
		}			
	});
	

    if(i == 0)
    {
        alert("You must map the headers.");
        return false;   
    }
    if(x == 0)
    {
        alert("You must map the employee name header.");
        return false;   
    }
    if(y == 0)
    {
         alert("You must map the email header.");
        return false;   
    }
    if(z == 0)
    {
        alert("You must map the designation header.");
        return false;   
    }

	return true;
}
</script>