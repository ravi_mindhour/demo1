<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Business_attribute_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
    }	
	
    public function get_business_attributes($condition_arr = array())
	{
		$this->db->select("*");
		if($condition_arr)
		{
			$this->db->where($condition_arr);
		}
		$this->db->order_by("display_name","asc");
		return $this->db->get("business_attribute")->result_array();
	}

	public function insert_business_attributes($db_arr)
	{	
		$this->db->insert("business_attribute",$db_arr);
	}

	public function update_business_attributes($db_arr,$condition_arr)
	{
		$this->db->where($condition_arr);
		$this->db->update('business_attribute', $db_arr);
	}
}