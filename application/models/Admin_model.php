<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
		date_default_timezone_set('Asia/Calcutta');
    }	    

	public function get_staff_list()
	{	
		$this->db->select("id, email, name, desig, status");
		return $this->db->from("login_user")->get()->result_array();
	}

	public function get_staff_list_by_uploaded_id($condition_arr)
	{	
		$this->db->select("login_user.id, login_user.email, login_user.name, login_user.desig, login_user.status");
		$this->db->from("tuple");
		$this->db->where($condition_arr);
		$this->db->join("login_user","login_user.id = tuple.user_id");
		return $this->db->get()->result_array();
	}

	public function get_designation_list()
	{	
		$this->db->select("*");
		return $this->db->where(array("status"=>1, "id >"=>1))->get("manage_designation")->result_array();
	}	

	public function insert_staff($staff_db_arr)
	{	
		$this->db->insert("login_user",$staff_db_arr);

	}

	public function get_country_list()
	{	
		$this->db->select("*");
		return $this->db->from("manage_country")->get()->result_array();
	}

	public function insert_country($country_db_arr)
	{	
		$this->db->insert("manage_country",$country_db_arr);
	}

	public function get_business_attributes_list()
	{	
		$this->db->select("*");
		$this->db->from("business_attribute");
		$this->db->order_by("type","asc");
		$this->db->order_by("display_name","asc");

		return $this->db->get()->result_array();
	}

	
	
	     
}