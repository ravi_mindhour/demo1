<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Approvel_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
		date_default_timezone_set('Asia/Calcutta');
    }	

    public function create_approvel_req($upload_id,$user_id)
	{	
		$approvel_stag = 1;
		$approver_id = $user_id;

		$result = $this->db->select("id")->where(array("company_Id"=>$this->session->userdata("companyid_ses"), "desig"=>$this->session->userdata("role_ses"), "id !=" =>$user_id))->get("login_user")->row_array();

		if($result)
		{
			$approver_id = $result["id"];			
		}
		
		$this->db->insert("approvel_requests",array("upload_id"=>$upload_id,"user_id"=>$approver_id,"approvel_stag"=>$approvel_stag,"createdon"=>date("Y-m-d H:i:s"), "type"=>1));		
	}

    public function create_rule_approvel_req($upload_id,$user_id)
	{	
		$approvel_stag = 1;
		$approver_id = $user_id;

		$result = $this->db->select("id")->where(array("company_Id"=>$this->session->userdata("companyid_ses"), "desig"=>$this->session->userdata("role_ses"), "id !=" =>$user_id))->get("login_user")->row_array();

		if($result)
		{
			$approver_id = $result["id"];			
		}
		
		$this->db->insert("approvel_requests",array("upload_id"=>$upload_id,"user_id"=>$approver_id,"approvel_stag"=>$approvel_stag,"createdon"=>date("Y-m-d H:i:s"), "type"=>2));		
	}	

	public function update_approvel_req($upload_id,$user_id)
	{	
		$flag = 0;
		$result = $this->db->select("*")->where(array("user_id"=>$user_id,"upload_id"=>$upload_id,"status"=>0, "type"=>1))->get("approvel_requests")->row_array();
		if($result)
		{
			$this->db->update("approvel_requests", array('status'=>1,"updatedon"=>date("Y-m-d H:i:s")), array("id"=>$result["id"]));
			$flag = 1;			
		}
		return $flag;
	}

	public function update_rule_approvel_req($upload_id,$user_id)
	{	
		$flag = 0;
		$result = $this->db->select("*")->where(array("user_id"=>$user_id,"upload_id"=>$upload_id,"status"=>0, "type"=>2))->get("approvel_requests")->row_array();
		if($result)
		{
			$this->db->update("approvel_requests", array('status'=>1,"updatedon"=>date("Y-m-d H:i:s")), array("id"=>$result["id"]));
			$flag = 1;			
		}
		return $flag;
	}


	public function get_approvel_request_list($user_id)
	{	
		$this->db->select("performance_cycle.id, performance_cycle.name, performance_cycle.start_date, performance_cycle.end_date, performance_cycle.description, approvel_requests.createdon as request_date, approvel_requests.upload_id");
		$this->db->from("approvel_requests");
		$this->db->join("data_upload","data_upload.id = approvel_requests.upload_id");
		$this->db->join("performance_cycle","data_upload.performance_cycle_id = performance_cycle.id");
		$result = $this->db->where(array("approvel_requests.user_id"=>$user_id,"approvel_requests.status"=>0, "approvel_requests.type"=>1))->get()->result_array();		
		return $result;
	}

	public function create_update_approvel_req($upload_id,$user_id)
	{	
		$approvel_stag = 1;
		$approver = CV_FIRST_APPROVER;
		$result = $this->db->select("*")->where(array("user_id"=>$user_id,"upload_id"=>$upload_id,"status"=>0))->get("approvel_requests")->row_array();
		if($result)
		{
			$this->db->update("approvel_requests", array('status'=>1,"updatedon"=>date("Y-m-d H:i:s")), array("id"=>$result["id"]));	
			if($result["approvel_stag"]==1)
			{
				$approvel_stag = 2;
				$approver = CV_SECOND_APPROVER;
			}
			elseif ($result["approvel_stag"]==2)
			{
				$approvel_stag = 3;
				$approver = CV_THIRD_APPROVER;
			}
			elseif ($result["approvel_stag"]==3)
			{
				$approvel_stag = 4;
				$approver = CV_FOURTH_APPROVER;
			}
			elseif ($result["approvel_stag"]==4)
			{
				$approvel_stag = 5;// Final approved
			}
		}
		if($approvel_stag < 5)
		{
			$this->db->select("datum.uploaded_value")->from("datum");
			$this->db->join("business_attribute","business_attribute.id = datum.business_attribute_id");
			$result1 = $this->db->where(array("business_attribute.module_name"=>$approver, "data_upload_id"=>$upload_id))->get()->row_array();
			if($result1)
			{				
				$result2 = $this->db->select("id")->where(array("company_Id"=>$this->session->userdata("companyid_ses"),"email"=>$result1["uploaded_value"]))->get("login_user")->row_array();
				if($result2)
				{
					$this->db->insert("approvel_requests",array("upload_id"=>$upload_id,"user_id"=>$result2["id"],"approvel_stag"=>$approvel_stag,"createdon"=>date("Y-m-d H:i:s")));
				}
			}

		}

	}

	public function fetchDatum_with_attributes($upload_id)
	{		
		$this->db->select("datum.*, business_attribute.module_name, business_attribute.id as attribute_id");
		$this->db->from("datum");
		$this->db->join("business_attribute","business_attribute.id = datum.business_attribute_id");
		return  $this->db->where(array("datum.data_upload_id"=>$upload_id))->get()->result_array();			
	}

	public function get_rules_approvel_request_list($user_id)
	{	
		$this->db->select("performance_cycle.id, performance_cycle.name, performance_cycle.start_date, performance_cycle.end_date, performance_cycle.description, approvel_requests.createdon as request_date, approvel_requests.upload_id");
		$this->db->from("approvel_requests");
		$this->db->join("data_upload","data_upload.id = approvel_requests.upload_id");
		$this->db->join("performance_cycle","data_upload.performance_cycle_id = performance_cycle.id");
		$result = $this->db->where(array("approvel_requests.user_id"=>$user_id,"approvel_requests.status"=>0, "approvel_requests.type"=>2))->get()->result_array();		
		return $result;

	/*	$this->db->from("hr_parameter");
		$this->db->join("data_upload","data_upload.id = hr_parameter.upload_id");
		$this->db->join("performance_cycle","data_upload.performance_cycle_id = performance_cycle.id");
		$result = $this->db->where(array("performance_cycle.status"=>5,"hr_parameter.status"=>2))->get()->result_array();
		return $result;*/
	}

	public function check_approvel_request_is_valid_for_user($condition_arr)
	{	
		$this->db->select("id");
		$this->db->from("approvel_requests");	
		return $this->db->where($condition_arr)->get()->row_array();	
	}

	
	
	     
}