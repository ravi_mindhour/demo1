<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Front_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();

        $dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
		date_default_timezone_set('Asia/Calcutta');		
    }
   
	public function is_valid_user($emailid, $pass, $company_id)
	{
		$this->db->select("login_user.id, login_user.email, login_user.name, login_user.desig");
		$this->db->from("login_user");
		$this->db->where("email = '$emailid' and pass = '$pass' and login_user.status =1 and login_user.company_Id = '$company_id'");
		$result = $this->db->get()->row_array();   
		if($result)
		{
        	return  $result ;
		}
	}

	public function get_staff_list()
	{	
		$this->db->select("id, email, name, desig, status");
		return $this->db->from("login_user")->get()->result_array();
	}

	public function get_dashboard_statics()
	{	
		$this->db->select("count(id) as performance_cycle, (select count(*) from login_user where desig='Admin') as hr_admin_user, (select count(*) from login_user where desig='HR') as hr_user, (select count(*) from login_user where desig='Employee') as employee,");
		return $this->db->from("performance_cycle")->get()->row_array();
	}

	public function get_employees_by_uploaded_id_list($upload_id)
	{	
		$this->db->select("login_user.*");
		$this->db->from("tuple");
		$this->db->join("login_user","tuple.user_id = login_user.id");
		$this->db->where("data_upload_id = '$upload_id'");
		return $this->db->get()->result_array();
	}


	public function get_user_dtl($condition_arr)
	{
		$this->db->select("login_user.id, login_user.email, login_user.name, login_user.desig");
		$this->db->from("login_user");
		$this->db->where($condition_arr);
		return $this->db->get()->row_array();   
	}
      
}