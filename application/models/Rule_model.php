<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Rule_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
		date_default_timezone_set('Asia/Calcutta');
    }	

    public function get_rule_dtls_for_performance_cycles($condition_arr)
	{	
		$this->db->select("*");
		$this->db->from("hr_parameter");
		return $this->db->where($condition_arr)->get()->row_array();
	}	

	public function insert_rules($data)
	{
		$this->db->insert("hr_parameter",$data);
		return $this->db->insert_id();
	}

	public function update_rules($condition_arr,$setData)
	{		
		$this->db->where($condition_arr);
		$this->db->update('hr_parameter', $setData);
	}

	public function insert_emp_salary_dtls($data)
	{
		$this->db->insert("employee_salary_details",$data);
		return $this->db->insert_id();
	}

     public function get_employee_salary_dtls($condition_arr)
	{	
		$this->db->select("login_user.name, login_user.email, login_user.desig, `upload_id`, `user_id`, `increment_applied_on_salary`, `performnace_based_increment`, `performnace_based_salary`, `crr_based_increment`, `crr_based_salary`, `standard_promotion_increase`, `final_salary`");
		$this->db->from("employee_salary_details");
		$this->db->join("login_user","login_user.id = employee_salary_details.user_id");
		if($condition_arr)
		{
			$this->db->where($condition_arr);
		}
		return $this->db->get()->row_array();
	}  

    public function get_ratings_list($upload_id, $condition_in_arr)
	{	
		$atti_ids = $this->array_value_recursive('id', $condition_in_arr);
		$this->db->select("business_attribute_id, uploaded_value, display_name_override");
		$this->db->from("datum");
		$this->db->where("data_upload_id",$upload_id);
		return $this->db->where_in("business_attribute_id",$atti_ids)->group_by("uploaded_value")->get()->result_array();
	}

    public function get_market_salary_header_list($upload_id, $condition_in_arr)
	{	
		$atti_ids = $this->array_value_recursive('id', $condition_in_arr);
		$this->db->select("business_attribute_id, display_name_override");
		$this->db->from("datum");
		$this->db->where("data_upload_id",$upload_id);
		return $this->db->where_in("business_attribute_id",$atti_ids)->group_by("display_name_override")->get()->result_array();
	}	

	public function array_value_recursive($key, array $arr)
	{
		$val = array();
		array_walk_recursive($arr, function($v, $k) use($key, &$val){
			if($k == $key) array_push($val, $v);
		});
		return count($val) > 1 ? $val : array_pop($val);
	}

	public function get_salary_elements_list($select_fields, $condition_arr)
	{	
		$this->db->select($select_fields);
		$this->db->from("business_attribute");
		return $this->db->where($condition_arr)->get()->result_array();
	}

	/*public function get_market_salary_elements_list($select_fields, $condition_arr)
	{	
		$this->db->select($select_fields);
		$this->db->from("business_attribute");
		return $this->db->where_in("module_name",$condition_arr)->get()->result_array();
	}*/

	public function get_user_performance_ratings($upload_id, $user_id,  $condition_arr=array())
	{
		if($condition_arr)
		{
			$rating_elements_list = $this->get_salary_elements_list("id", $condition_arr);
		}
		$this->db->select("datum.*");
		$this->db->from("tuple");
		$this->db->join("datum","datum.row_num = tuple.row_num");
		if($condition_arr)
		{
			return $this->db->where(array("tuple.user_id"=>$user_id,"tuple.data_upload_id"=>$upload_id, "datum.data_upload_id"=>$upload_id, "datum.business_attribute_id"=>$rating_elements_list[0]["id"]))->get()->result_array();
		}
		else
		{
			return $this->db->where(array("tuple.user_id"=>$user_id,"tuple.data_upload_id"=>$upload_id, "datum.data_upload_id"=>$upload_id))->get()->result_array();
		}
	}

	public function get_user_cell_value_frm_datum($upload_id, $user_id, $condition_arr=array())
	{		
		$this->db->select("datum.*");
		$this->db->from("tuple");
		$this->db->join("datum","datum.row_num = tuple.row_num");
	return $this->db->where(array("tuple.user_id"=>$user_id,"tuple.data_upload_id"=>$upload_id, "datum.data_upload_id"=>$upload_id))->where($condition_arr)->get()->row_array();
	}


	     
}