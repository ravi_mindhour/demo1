<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Messages_model extends CI_MODEL
{
	public function __construct()
	{
		parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
    }
	
    public function insertMessage($msg_db_arr)
    {
    	$sql = "insert into messages(message_text,message_definition_message_num,message_type_code,data_upload_id,datum_col_num,datum_row_num,tuple_row_num) values(?,?,?,?,?,?,?)" ;
		$query = $this->db->query($sql, $msg_db_arr);
		return $query;
	}

	public function getTuple($upload_id)
	{
		$sql = "select row_num from tuple where data_upload_id = '".$upload_id."' ";
		$query = $this->db->query($sql);
		foreach($query->result() as  $row){
			$id = $row->row_num;
		}
		return $id;
	}
	
	
	



}?>