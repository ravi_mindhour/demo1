<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Manager_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
    }

    public function array_value_recursive($key, array $arr)
	{
		$val = array();
		array_walk_recursive($arr, function($v, $k) use($key, &$val){
			if($k == $key) array_push($val, $v);
		});
		return count($val) > 1 ? $val : array_pop($val);
	}

    public function get_performance_cycles_list()
	{
		$user_email = $this->session->userdata('email_ses');
		$upload_ids_arr = $this->db->select("upload_id")->where("first_approver", $user_email)->or_where("second_approver", $user_email)->or_where("third_approver", $user_email)->or_where("fourth_approver", $user_email)->get("row_owner")->result_array();

		$upload_ids = $this->array_value_recursive('upload_id', $upload_ids_arr);
		$this->db->select("performance_cycle.*, data_upload.id as upload_id");
		$this->db->from("performance_cycle");
		$this->db->join("data_upload","data_upload.performance_cycle_id = performance_cycle.id");
		$this->db->where(array("performance_cycle.status"=>7));
		$this->db->where_in("data_upload.id",$upload_ids);
		return $this->db->get()->result_array();	
	}
	
	public function get_performance_cycles($condition_arr)
	{
		return $this->db->select("performance_cycle.*, (SELECT `id` FROM `data_upload` WHERE `data_upload`.`performance_cycle_id` = performance_cycle.`id` order by `data_upload`.`id` asc limit 1) as upload_id")->where($condition_arr)->get("performance_cycle")->result_array();		
	}

	public function get_upload_id_by_performance_cycle_id($condition_arr)
	{	
		$this->db->select("id");
		$this->db->from("data_upload");
		return $this->db->where($condition_arr)->order_by('id','desc')->get()->row_array();
	}

	public function get_employees_list_by_uploaded_id($upload_id)
	{	
		$user_email = $this->session->userdata('email_ses');
		$where_str = "(first_approver = '$user_email' or second_approver= '$user_email' or third_approver = '$user_email' or fourth_approver = '$user_email')";
		$this->db->select("login_user.*");
		$this->db->from("row_owner");
		$this->db->join("tuple","row_owner.row_id = tuple.row_num");
		$this->db->join("login_user","tuple.user_id = login_user.id");
		$this->db->where(array("row_owner.upload_id" => $upload_id, "tuple.data_upload_id" => $upload_id));
		$this->db->where($where_str);
		return $this->db->get()->result_array();
	}

	public function get_employee_salary_dtls($condition_arr)
	{	
		$this->db->select("login_user.name, login_user.email, login_user.desig, `upload_id`, `user_id`, `increment_applied_on_salary`, `performnace_based_increment`, `performnace_based_salary`, `crr_based_increment`, `crr_based_salary`, `standard_promotion_increase`, `final_salary`");
		$this->db->from("employee_salary_details");
		$this->db->join("login_user","login_user.id = employee_salary_details.user_id");
		if($condition_arr)
		{
			$this->db->where($condition_arr);
		}
		return $this->db->get()->row_array();
	}  
	
	     
}