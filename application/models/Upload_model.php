<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$dbname = $this->session->userdata("dbname_ses");
		if(trim($dbname))
		{
			$this->db->query("Use $dbname");		
		}
    }
	
	public function insert_performance_cycle($data)
	{
		$this->db->insert("performance_cycle",$data);
		return $this->db->insert_id();//($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	public function get_performance_cycles($condition_arr)
	{
		return $this->db->select("performance_cycle.*, (SELECT `id` FROM `data_upload` WHERE `data_upload`.`performance_cycle_id` = performance_cycle.`id` order by `data_upload`.`id` asc limit 1) as upload_id")->where($condition_arr)->get("performance_cycle")->result_array();		
	}
	public function insert_uploaded_file_dtls($data)
	{
		$this->db->insert("data_upload",$data);
		return $this->db->insert_id();
	}
	
	public function insert_Bussiness_Attri($data)
	{
		$this->db->insert("business_attribute",$data);
		//return $this->db->insert_id();
	}
	
	public function update_Bussiness_Attri($module,$id)
	{		
		$this->db->where('id', $id);
		$this->db->update('business_attribute', array('module_name' => $module));
	}
   
	public function insert_tuples($data)
	{
		$this->db->insert("tuple",$data);
		return $this->db->insert_id();
	} 

	public function get_tuples($condition_arr)
	{
		return $this->db->select("id")->where($condition_arr)->get("tuple")->result_array();
	} 
	   
 /*   public function fetchHeaders($upload_id)
	{
		return $this->db->select("id,display_name")->where("upload_id",$upload_id)->get("business_attribute")->result_array();
	}*/
	
	public function fetchHeaders($upload_id)
	{
		return $this->db->select("*")->where(array("data_upload_id"=>$upload_id, "row_num"=>0))->get("datum")->result_array();
	}

	public function get_upload_id_by_performance_cycle_id($condition_arr)
	{	
		$this->db->select("id");
		$this->db->from("data_upload");
		return $this->db->where($condition_arr)->order_by('id','desc')->get()->row_array();
	}
	
	public function get_uploaded_file_dtls($upload_id)
	{
		return $this->db->select("id,original_file_name,performance_cycle_id,uploaded_by_user_id")->where("id",$upload_id)->get("data_upload")->row_array();
	}
	
	public function check_module($module_name,$module_value,$row_num,$upload_id)
	{	
		if($module_name != "N/A" and $module_name != "" and $module_value != "")
		{
			if($module_name == CV_EMAIL_MODULE_NAME)
			{			
				/*$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
				$pass = array(); //remember to declare $pass as an array
				$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
				for ($i = 0; $i < 8; $i++)
				{
					$n = rand(0, $alphaLength);
					$pass[] = $alphabet[$n];
				}*/
				$pass = "12345";//implode($pass);			
				$this->db->select('id');			
				$this->db->from('login_user');
				$this->db->where('email',$module_value);
				$query=$this->db->get()->row_array();
				
				//$count = $query->num_rows();
				if(!$query)
				{				
					$sql = "insert into login_user (email,pass,desig,status,company_Id) values(?,?,?,?,?)";	
					$query = $this->db->query($sql,array($module_value,md5($pass),"Employee","1",CV_COMPANY_ID));
					$user_id = $this->db->insert_id();				
				}
				else
				{
					$user_id = $query["id"];
				}
				$this->db->update("tuple", array('tuple.user_id'=>$user_id), array("tuple.data_upload_id"=>$upload_id, "tuple.row_num"=>$row_num));
			}
			elseif($module_name == CV_FIRST_APPROVER or $module_name == CV_SECOND_APPROVER or $module_name == CV_THIRD_APPROVER or $module_name == CV_FOURTH_APPROVER)
			{							
				$this->db->select('id');			
				$this->db->from('login_user');
				$this->db->where('email',$module_value);
				$query=$this->db->get()->row_array();
				
				if(!$query)
				{	
					/*$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
					$pass = array(); //remember to declare $pass as an array
					$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
					for ($i = 0; $i < 8; $i++)
					{
						$n = rand(0, $alphaLength);
						$pass[] = $alphabet[$n];
					}*/
					$pass = "12345";//implode($pass);

					$sql = "insert into login_user (email,pass,desig,status,company_Id) values(?,?,?,?,?)";	
					$query = $this->db->query($sql,array($module_value,md5($pass),"Manager","1",CV_COMPANY_ID));	
				}				

				$this->db->select('id')->from('row_owner');
				$this->db->where(array('upload_id'=>$upload_id, 'row_id'=>$row_num));
				$row_owner_result = $this->db->get()->row_array();
				if($row_owner_result)
				{
					$this->db->update("row_owner", array($module_name => $module_value), array('id'=>$row_owner_result['id']));
				}
			}
			else
			{
				$result = "manage_".$module_name;				
				if($this->db->table_exists($result))
				{
					$this->db->select('name');
					$this->db->from($result);
					$this->db->where('name',$module_value);
					$query=$this->db->get();
					$count = $query->num_rows();	
					if($count == 0 )
					{
						$sql = "insert into ".$result." (name) values(?)";
						$query = $this->db->query($sql,array($module_value));
					}
				}
			}		
		}	
	}
	
	public function setUserBranch($upload_id, $row_no, $branchId)
	{
		$sql = "select uploaded_value from datum where business_attribute_id = (select distinct bussines_attribut from data_mapping where upload_id = '$upload_id' and mapped = 'email') and row_num = '$row_no' ";		
		$query = $this->db->query($sql);		
		foreach($query->result() as $row)
		{
			$userId = $row->uploaded_value;
		}		
		$sql = "insert into manage_user_branch (upload_id,user_id,branch_id) values('$upload_id','$userId',(select id from manage_branch where name = '".$branchId."')) ";
		$query = $this->db->query($sql);
		return $query;	
	}
	
	public function insertHeaders($datum_db_arr,$data_mappin_db_arr)
	{		
		$sql = "insert into datum(row_num,col_num,data_upload_id,display_name_override,uploaded_value,value,datum_status_code,business_attribute_id) values(?,?,?,?,?,?,?,?)" ;
		$query1 = $this->db->query($sql, $datum_db_arr);
		
		if($data_mappin_db_arr)
		{
			$sql="insert into data_mapping (upload_id,bussines_attribut,mapped) values(?,?,?)" ;
			$query = $this->db->query($sql, $data_mappin_db_arr);
		}
		return $query1;		
	}
	
	public function insertRowOwner($rowOwner_db_arr)
	{
		$sql = "insert into row_owner (row_id,upload_id,first_approver,second_approver,third_approver,fourth_approver) values(?,?,?,?,?,?)";
		$query = $this->db->query($sql,$rowOwner_db_arr);
		return $query;
	}

	public function fetchDatum($upload_id)
	{		
		$sql = "select * from datum where data_upload_id = '".$upload_id."'";
		$query = $this->db->query($sql);
		return $query->result_array();		
	}

	public function update_performance_cycle_status($upload_id, $user_id, $status)
	{	
		$result = $this->db->select("performance_cycle_id")->where(array("id"=>$upload_id))->get("data_upload")->row_array();
		if($result)
		{
			$this->db->update("performance_cycle", array('performance_cycle.status'=>$status, "performance_cycle.updatedby"=>$user_id,"performance_cycle.updatedon"=>date("Y-m-d H:i:s")), array("performance_cycle.id"=>$result["performance_cycle_id"]));
		}
	}
	
	     
}