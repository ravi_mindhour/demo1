<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rules extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("rule_model");
		$this->load->model("upload_model");
		$this->load->model("approvel_model");
		$this->load->model("front_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	public function create_rules($performance_cycle_id)
	{
		$data["performance_cycle_list"] = $this->upload_model->get_performance_cycles(array("id"=>$performance_cycle_id, "status"=>4));
		$data['title'] = "Create Rules";
		$data['body'] = "create_rules";
		$this->load->view('common/structure',$data);
	}

	public function get_performance_cycle_dtls()
	{
		if($this->input->post("pid"))
		{
			$data["performance_cycle_id"] = $this->input->post("pid");

			/*$upload_dtl_arr = $this->upload_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$data["performance_cycle_id"]));

			$rating_elements_list = $this->rule_model->get_salary_elements_list("id",array("module_name"=>CV_RATING_ELEMENT));

			$data["ratings"] = $this->rule_model->get_ratings_list($upload_dtl_arr["id"], $rating_elements_list);*/

			//$data["salary_elements_list"] = $this->rule_model->get_salary_elements_list("id, display_name, module_name", array("module_name"=>CV_SALARY_ELEMENT));			

			$data["rule_dtls"] = $this->rule_model->get_rule_dtls_for_performance_cycles(array("performance_cycle_id"=>$this->input->post("pid")));

			/*if($data["rule_dtls"])
			{
				$upload_dtl_arr = $this->upload_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$data["performance_cycle_id"]));

				$rating_elements_list = $this->rule_model->get_salary_elements_list("id",array("module_name"=>CV_RATING_ELEMENT));

				$data["ratings"] = $this->rule_model->get_ratings_list($upload_dtl_arr["id"], $rating_elements_list);

				echo "<pre>";print_r($data["market_salary_elements_list"] );die;
			}
*/

			$this->load->view('create_rules_partial',$data);
		}
	}	

	public function set_rules($step=0)
	{
		if($step>0 and $this->input->post("hf_performance_cycle_id") > 0)
		{
			$upload_dtl_arr = $this->upload_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$this->input->post("hf_performance_cycle_id")));

			if($step==1)
			{
				$performance_cycle_arr = $this->upload_model->get_performance_cycles(array("id"=>$this->input->post("hf_performance_cycle_id")));

				$start_dt = strtotime($performance_cycle_arr[0]["start_date"]);
				$end_dt = strtotime($performance_cycle_arr[0]["end_date"]);

				$year1 = date('Y', $start_dt);
				$year2 = date('Y', $end_dt);

				$month1 = date('m', $start_dt);
				$month2 = date('m', $end_dt);

				$diff_month = (($year2 - $year1) * 12) + ($month2 - $month1);

				$step1_db_arr = array(
								'performance_cycle_id' => $this->input->post("hf_performance_cycle_id"),
								'include_inactive' => $this->input->post("ddl_include_inactive"),
								'prorated_increase' => $this->input->post("ddl_prorated_increase"),
								//'fiscal_year' => $this->input->post("ddl_fiscal_year"),
								//'increase_applied_salary_element' => $this->input->post("ddl_salary_element"),
								'Manager_discretionary_increase' => $this->input->post("txt_manager_discretionary_increase"),
								'Manager_discretionary_decrease' => $this->input->post("txt_manager_discretionary_decrease"),
								'upload_id' => $upload_dtl_arr["id"],
								'start_date' => $performance_cycle_arr[0]["start_date"],
								'end_date' => $performance_cycle_arr[0]["end_date"],
								'increase_applicable_for_month' => $diff_month,
								'status' => 1,
								'createdby' => $this->session->userdata('userid_ses'),
								'createdon' => date("Y-m-d H:i:s"));

				$rating_arr = array();
				if($this->input->post("ddl_performnace_based_hikes")=='yes')
				{
					foreach($_REQUEST['hf_rating_name'] as $key=> $value) 
					{ 
						if($value !='') 
						{ 
							$rating_arr[$value] = $_REQUEST['ddl_market_salary_rating'][$key];
						}
					}
				}
				else
				{
					$rating_arr['all'] = $this->input->post("txt_rating1");
				}

				$step1_db_arr['performnace_based_hike'] = $this->input->post("ddl_performnace_based_hikes");
				$step1_db_arr['performnace_based_hike_ratings'] = json_encode($rating_arr);

				$comparative_ratio_arr = array();
				if($this->input->post("ddl_comparative_ratio")=='yes')
				{					
					foreach($_REQUEST['hf_comparative_ratio_name'] as $key=> $value) 
					{ 
						if($value !='') 
						{ 
							$comparative_ratio_arr[$value] = $_REQUEST['ddl_market_salary_comparative_ratio'][$key];
						}
					}
				}
				else
				{
					$comparative_ratio_arr['all'] = $this->input->post("ddl_comparative_ratio1");
				}
				$step1_db_arr['comparative_ratio'] = $this->input->post("ddl_comparative_ratio");
				//$step1_db_arr['comparative_ratio_salary_element'] = $this->input->post("ddl_comparative_ratio_salary_element");
				$step1_db_arr['comparative_ratio_calculations'] = json_encode($comparative_ratio_arr);
				
				$crr_arr = array();
				$min_range = 0;
				$cnt = 0;
				foreach($_REQUEST['txt_crr'] as $key=> $value) 
				{ 
					if($value !='') 
					{ 
						$crr_arr["range".($cnt+1)] = array("min"=>$min_range,"max"=>$value);
						$min_range = $value;
						$cnt++;
					}
				}
				$step1_db_arr['comparative_ratio_range'] = json_encode($crr_arr);
				echo $rule_id = $this->rule_model->insert_rules($step1_db_arr);
				echo true; die;
			}
			elseif($step==2 and $this->input->post("hf_rule_id") > 0)
			{
				$crr_percent_values_arr = array();
				for($i=0; $i<$this->input->post("hf_crr_counts"); $i++)
				{
					$crr_percent_values_arr[$i] = $_REQUEST['txt_range_val'.$i];
				}

				//echo "<pre>";print_r(json_encode($crr_percent_values_arr));die;
				

				$step2_db_arr = array(
								'overall_budget' => $this->input->post("ddl_overall_budget"),
								'budget_amount' => $this->input->post("txt_budget_amt"),
								'budget_percent' => $this->input->post("txt_budget_percent"),
								'standard_promotion_increase' => $this->input->post("txt_standard_promotion_increase"),
								'Overall_maximum_age_increase' =>json_encode($_REQUEST['txt_max_hike']),
								'if_recently_promoted' =>json_encode($_REQUEST['txt_recently_promoted_max_salary_increase']),
								'crr_percent_values' => json_encode($crr_percent_values_arr),
								'status' => 2);
				$this->rule_model->update_rules(array("id"=>$this->input->post("hf_rule_id"), "performance_cycle_id"=>$this->input->post("hf_performance_cycle_id")), $step2_db_arr);
				$this->upload_model->update_performance_cycle_status($upload_dtl_arr["id"], $this->session->userdata("userid_ses"), 5);
				
				$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>Rules created successfully.</b></span></div>'); 
				echo 2; die;
			}			
		}
	}

	public function view_rule_budget($upload_id)
	{
		
		$data = $this->calculate_increments($upload_id,0);
		$data['upload_id'] = $upload_id;
		$data['title'] = "View Rule Budget";
		$data['body'] = "view_rule_budget";
		$this->load->view('common/structure',$data);
	}

	public function send_rule_approval_request($upload_id)
	{
		$this->calculate_increments($upload_id,1);// Insert emp salary as rule
		$this->approvel_model->create_rule_approvel_req($upload_id, $this->session->userdata("userid_ses"));
		$this->upload_model->update_performance_cycle_status($upload_id, $this->session->userdata("userid_ses"), 6);
		$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>Rules sent for approval successfully.</b></span></div>'); 
		redirect(site_url("performance-cycle"));
	}

	public function calculate_increments($upload_id, $is_need_to_save_data=0)
	{	
		// $is_need_to_save_data = 0 (No) and 1 (Yes)
		$rule_dtls = $this->rule_model->get_rule_dtls_for_performance_cycles(array("upload_id"=>$upload_id));

		if($rule_dtls["overall_budget"]=="Automated locked" or $rule_dtls["overall_budget"]=="Automated but x% can exceed")
		{
			$data['total_max_budget'] = $rule_dtls['budget_amount'] + (($rule_dtls['budget_amount']*$rule_dtls['budget_percent'])/100);
		}
		elseif($rule_dtls["overall_budget"]=="Automated but x% can exceed")
		{
			$data['total_max_budget'] = $rule_dtls['budget_amount'];
		}
		else
		{
			$data['total_max_budget'] = 0; // No limit set for budget
		}		

		$all_emp_total_salary = 0;

		$staff_list = $this->front_model->get_employees_by_uploaded_id_list($upload_id);

		foreach($staff_list as $row)
		{
			$increment_applied_on_arr = $this->rule_model->get_user_performance_ratings($upload_id, $row["id"], array("module_name"=>CV_INCREMENT_APPLIED_ON));
			$increment_applied_on_amt = $increment_applied_on_arr[0]["uploaded_value"];

			$emp_salary_rating = $this->rule_model->get_user_performance_ratings($upload_id, $row["id"], array("module_name"=>CV_RATING_ELEMENT));
		
			$performnace_based_increment_percet=0;

			$rating_dtls = json_decode($rule_dtls["performnace_based_hike_ratings"],true);

			if($rule_dtls["performnace_based_hike"] == "yes")
			{
				foreach ($rating_dtls as $key => $value) 
				{
					$key_arr =  explode(CV_CONCATENATE_SYNTAX, $key);
					if($key_arr[1] == $emp_salary_rating[0]["value"])
					{
						$performnace_based_increment_percet = $value;
					}
				}			
			}
			else
			{
				$performnace_based_increment_percet = $rating_dtls["all"];
			}

			$performnace_based_salary = $increment_applied_on_amt + (($increment_applied_on_amt*$performnace_based_increment_percet)/100);

			$emp_market_salary = 0;
			$comparative_ratio_dtls = json_decode($rule_dtls["comparative_ratio_calculations"],true);

			if($rule_dtls["comparative_ratio"] == "yes")
			{
				foreach ($comparative_ratio_dtls as $key => $value) 
				{
					$key_arr =  explode(CV_CONCATENATE_SYNTAX, $key);
					if($key_arr[1] == $emp_salary_rating[0]["value"])
					{
						$val_arr =  explode(CV_CONCATENATE_SYNTAX, $value);
						$emp_market_salary_arr = $this->rule_model->get_user_cell_value_frm_datum($upload_id, $row["id"], array("business_attribute_id"=>$val_arr[0]));
						$emp_market_salary = $emp_market_salary_arr['uploaded_value'];
					}
				}			
			}
			else
			{
				$val_arr =  explode(CV_CONCATENATE_SYNTAX, $comparative_ratio_dtls["all"]);
				$emp_market_salary_arr = $this->rule_model->get_user_cell_value_frm_datum($upload_id, $row["id"], array("business_attribute_id"=>$val_arr[0]));
				$emp_market_salary = $emp_market_salary_arr['uploaded_value'];
			}

			$crr_per = $performnace_based_salary/$emp_market_salary;

			$i=0; $crr_arr = json_decode($rule_dtls["comparative_ratio_range"], true);
			foreach($crr_arr as $row1)
			{			
				if($i==0)
				{ 
					if($crr_per <= $row1["max"])
					{
						break;
					}
				}
				elseif(($i+1)==count($crr_arr))
				{
					if($row1["max"] > $crr_per)
					{
						break;
					}
				}
				else
				{
					if($row1["min"] < $crr_per and $crr_per <= $row1["max"])
					{
						break;
					}
				}
				$i++; 
			}

			$crr_tbl_arr_temp = json_decode($rule_dtls["crr_percent_values"], true);
			$crr_tbl_arr = $crr_tbl_arr_temp[$i];
			$rang_index = $i;	
			$crr_based_increment_percet = 0;
			$j=0;
			if($rule_dtls["comparative_ratio"] == "yes")
			{
				foreach ($comparative_ratio_dtls as $key => $value) 
				{
					$key_arr =  explode(CV_CONCATENATE_SYNTAX, $key);
					if($key_arr[1] == $emp_salary_rating[0]["value"])
					{
						$crr_based_increment_percet =  $crr_tbl_arr[$j];
					}
					$j++;
				}
			}
			else
			{
				$crr_based_increment_percet =  $crr_tbl_arr[$j];
			}

			$data['increment_applied_on_amt'] = $increment_applied_on_amt;
			$data['standard_promotion_increase_perc'] = $rule_dtls["standard_promotion_increase"];
			$data['performnace_based_increment_percet'] = $performnace_based_increment_percet;
			$data['performnace_based_salary'] = $performnace_based_salary;
			$data['crr_based_increment_percet'] = $crr_based_increment_percet;
			$data['crr_based_salary'] = $performnace_based_salary + (($performnace_based_salary*$crr_based_increment_percet)/100);
			$data['final_salary'] = $data['crr_based_salary'] + (($data['crr_based_salary']*$data['standard_promotion_increase_perc'] )/100);

			$all_emp_total_salary += $data['final_salary'];

			if($is_need_to_save_data==1)
			{
				$db_arr =array(
							"upload_id" => $upload_id,
							"user_id" => $row["id"],
							"increment_applied_on_salary" => $increment_applied_on_amt,
							"performnace_based_increment" => $performnace_based_increment_percet,
							"performnace_based_salary" => $performnace_based_salary,
							"crr_based_increment" => $crr_based_increment_percet,
							"crr_based_salary" => $data['crr_based_salary'],
							"standard_promotion_increase" => $data['standard_promotion_increase_perc'],
							"final_salary" => $data['final_salary'],
							"created_by" => $this->session->userdata('userid_ses'),
							"created_on" =>date("Y-m-d H:i:s"));
				$this->rule_model->insert_emp_salary_dtls($db_arr);
			}
		}
		
		return array("total_max_budget" => $data['total_max_budget'], "all_emp_total_salary" => $all_emp_total_salary);
	}	

	public function get_ratings_list()
	{
		if($this->input->post("pid"))
		{
			$ddl_name = "ddl_market_salary_".$this->input->post("txt_name");
			$hf_name = "hf_".$this->input->post("txt_name")."_name";

			$upload_dtl_arr = $this->upload_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$this->input->post("pid")));

			if($this->input->post("txt_name")=="rating")
			{
				$ddl_str = '<input type="text" name="'.$ddl_name.'[]" class="form-control" required onKeyUp="validate_onkeyup(this);" onBlur="validate_onblure(this);" maxlength="5" />';
			}
			else
			{
				$market_salary_elements_list = $this->rule_model->get_salary_elements_list("id", array("module_name"=>CV_MARKET_SALARY_ELEMENT));
				$market_salary = $this->rule_model->get_market_salary_header_list($upload_dtl_arr["id"], $market_salary_elements_list);

				$ddl_str = '<select class="form-control" name="'.$ddl_name.'[]" required >';
				foreach ($market_salary as $row)
				{
					$ddl_str .= '<option value="'.$row["business_attribute_id"].''.CV_CONCATENATE_SYNTAX.''.$row['display_name_override'].'">'.$row["display_name_override"].'</option>';
				}
				$ddl_str .= '</select>';
			}

			$rating_elements_list = $this->rule_model->get_salary_elements_list("id",array("module_name"=>CV_RATING_ELEMENT));
			$ratings = $this->rule_model->get_ratings_list($upload_dtl_arr["id"], $rating_elements_list);

			foreach ($ratings as $row)
			{
				echo '<div class="form-group"><div class="row"><div class="col-sm-6">';
			    echo '<label class="control-label">'.$row['uploaded_value'].'</label></div>';
		        echo '<div class="col-sm-6">
		        		<input type="hidden" value="'.$row['business_attribute_id'].''.CV_CONCATENATE_SYNTAX.''.$row['uploaded_value'].'" name="'.$hf_name.'[]" />
		                '.$ddl_str.'
		        	</div></div>
					</div>';
			}
		}
	}

	public function get_comparative_ratio_element_for_no()
	{
		if($this->input->post("pid"))
		{
			$upload_dtl_arr = $this->upload_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$this->input->post("pid")));

			$market_salary_elements_list = $this->rule_model->get_salary_elements_list("id", array("module_name"=>CV_MARKET_SALARY_ELEMENT));
			$market_salary = $this->rule_model->get_market_salary_header_list($upload_dtl_arr["id"], $market_salary_elements_list);

			$ddl_str = '';
			foreach ($market_salary as $row)
			{
				$ddl_str .= '<option value="'.$row["business_attribute_id"].''.CV_CONCATENATE_SYNTAX.''.$row['display_name_override'].'">'.$row["display_name_override"].'</option>';
			}
			echo $ddl_str;
		}
	}


	public function view_rule_details($pid=0)
	{
		$data["performance_cycle_list"] = $this->upload_model->get_performance_cycles(array("id"=>$pid));
		if($data["performance_cycle_list"])
		{
			$is_enable_approve_btn = 0; // No
			$request_dtl= $this->approvel_model->check_approvel_request_is_valid_for_user(array("upload_id"=>$data["performance_cycle_list"][0]["upload_id"], "approvel_requests.user_id"=>$this->session->userdata("userid_ses"),"approvel_requests.status"=>0, "approvel_requests.type"=>2));
			if($request_dtl)
			{
				$is_enable_approve_btn = 1; //Yes
			}
			$data["upload_id"] = $data["performance_cycle_list"][0]["upload_id"];
			$data['is_enable_approve_btn'] = $is_enable_approve_btn;			
			$data["rule_dtls"] = $this->rule_model->get_rule_dtls_for_performance_cycles(array("performance_cycle_id"=>$pid));
			$data['title'] = "View Rule";
			$data['body'] = "view_rule_details";
			$this->load->view('common/structure',$data);
		}
		else
		{
			redirect(site_url("performance-cycle"));
		}
	}	

}
