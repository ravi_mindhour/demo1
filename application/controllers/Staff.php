<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
		//$this->load->library('parser');
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("staff_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	public function index()
	{
		$data['msg'] = "";		
		$data['staff_list'] = $this->staff_model->get_staff_list();				
		$data['title'] = "Staffs";
		$data['body'] = "admin/staff_list";
		$this->load->view('common/structure',$data);		
	}

	public function add_staff()
	{
		$data['msg'] = "";					
		$data['title'] = "Add Staffs";
		$data['body'] = "admin/add_staff";
		$this->load->view('common/structure',$data);		
	}

}
