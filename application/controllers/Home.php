<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		$this->load->library('session', 'encrypt');			
		$this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');                              
    }
	
	public function index()
	{
		$data['msg'] = "";
		//$data['body'] = "index";
		if($this->input->post())
		{
			$this->form_validation->set_rules('email', 'EMAIL', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if($this->form_validation->run())
			{
				$uname = $this->input->post('email');
				$pass = md5($this->input->post("password"));

				$username_arr = explode('@',$uname);
				$domain_name = end($username_arr);

				$this->load->model("domain_model");
				$domain_dtls = $this->domain_model->is_valid_domain($domain_name);

				if($domain_dtls)
				{
					$this->session->set_userdata(array('dbname_ses' => $domain_dtls['dbname']));	

					$this->load->model("front_model");
					$result = $this->front_model->is_valid_user($uname,$pass, $domain_dtls['company_id']);
					if($result)
					{
						$detail = array(
							'userid_ses'=>$result['id'],
							'email_ses' => $result['email'],
							'username_ses' => $result['name'],
							'role_ses' => $result['desig'],
							'companyid_ses' => $domain_dtls['company_id'],
							'companyname_ses' => $domain_dtls['company_name'],
							'domainname_ses' => $domain_dtls['domainName']					
							);
						$this->session->set_userdata($detail);
						if($this->session->userdata('role_ses') == "Admin")
						{
							redirect(site_url("dashboard"));
						}
						elseif($this->session->userdata('role_ses') == "Manager")
						{
							redirect(site_url("manager/dashboard"));
						}
						elseif($this->session->userdata('role_ses') == "HR_Admin")
						{
							redirect(site_url("dashboard"));
						}
						elseif($this->session->userdata('role_ses') == "HR")
						{
							redirect(site_url("dashboard"));
						}	
						else
						{
							redirect(site_url("dashboard"));	
						}
						
					}
					else
					{
						$data["msg"]="Invalid email or password.";
					}
				}
				else
				{
					$data["msg"]="Invalid email or password..";
				}
			}
			else
			{
				$data['msg'] = validation_errors();
			}			
		}
		
		$data['title'] = "Home";
		$this->load->view('login',$data);
		//$this->load->view('common/structure',$data);
		
		/*$data = array('title'=> 'Home',
        'body' =>$this->parser->parse('index',array(),true));
        $this->parser->parse('common/structure', $data);*/
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
        setcookie(session_name(), "", time() - 3600);
		$data["msg"]="Logged out successfully.";	
		$data['title'] = "Home";
		$this->load->view('login',$data);
	}
}
