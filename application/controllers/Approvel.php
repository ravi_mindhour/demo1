<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Approvel extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
		//$this->load->library('parser');
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("approvel_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	/*public function index()
	{
		$data['approvel_request_list'] = $this->approvel_model->get_approvel_request_list($this->session->userdata("userid_ses"));
		$data['title'] = "Approvel Request List";
		$data['body'] = "approvel_request_list";
		//echo "<pre>";print_r($data);die;
		$this->load->view('common/structure',$data);
	}*/
	
	public function update_approvel_req($upload_id)
	{
		$result = $this->approvel_model->update_approvel_req($upload_id, $this->session->userdata("userid_ses"));
		if($result)
		{
			$this->load->model("upload_model");
			$datum_dtl = $this->approvel_model->fetchDatum_with_attributes($upload_id);
	
			foreach ($datum_dtl as $row) 
			{
				$this->upload_model->check_module($row["module_name"],$row["uploaded_value"],$row["row_num"], $upload_id);
			}

			$this->upload_model->update_performance_cycle_status($upload_id, $this->session->userdata("userid_ses"), 4);
			$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>Sheet approved successfully.</b></span></div>'); 
		}
		else
		{
			$this->session->set_flashdata('message', '<div align="left" style="color:red;" id="notify"><span><b>Sheet not approved. Try again!</b></span></div>');
		}

		redirect(site_url("performance-cycle"));
	}
	
	/*public function rules_approvel_list()
	{
		$data['rules_approvel_request_list'] = $this->approvel_model->get_rules_approvel_request_list($this->session->userdata("userid_ses"));
		$data['title'] = "Rules Approvel Request List";
		$data['body'] = "ruels_approvel_request_list";
		//echo "<pre>";print_r($data);die;
		$this->load->view('common/structure',$data);
	}*/

	public function update_rule_approvel_req($upload_id)
	{
		$result = $this->approvel_model->update_rule_approvel_req($upload_id, $this->session->userdata("userid_ses"));
		if($result)
		{
			$this->load->model("upload_model");
			$this->upload_model->update_performance_cycle_status($upload_id, $this->session->userdata("userid_ses"), 7);
			$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>Rule approved successfully.</b></span></div>'); 
		}
		else
		{
			$this->session->set_flashdata('message', '<div align="left" style="color:red;" id="notify"><span><b>Rule not approved. Try again!</b></span></div>');
		}
		redirect(site_url("performance-cycle"));
	}




}
