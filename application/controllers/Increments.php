<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Increments extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
	
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("rule_model");
		$this->load->model("front_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	                              
	public function view_increments($performance_cycle_id)
	{
		$this->load->model("upload_model");
		$data["performance_cycle_list"] = $this->upload_model->get_performance_cycles(array("id"=>$performance_cycle_id, "status"=>7));
		$data['title'] = "Increment Calculation";
		$data['body'] = "increment_index";
		$this->load->view('common/structure',$data);
	}

	public function get_staff_list()
	{
		if($this->input->post("pid"))
		{		
			$this->load->model("upload_model");
			$data["performance_cycle_id"] = $this->input->post("pid");
			$upload_dtl_arr = $this->upload_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$data["performance_cycle_id"]));			
			$data['upload_id'] = $upload_dtl_arr["id"];
			$data['staff_list'] = $this->front_model->get_employees_by_uploaded_id_list($upload_dtl_arr["id"]);	
			$this->load->view('increment_partial',$data);
		}
	}	

	public function view_emp_increments($upload_id, $uid)
	{	
		$data['salary_dtls'] = $this->rule_model->get_employee_salary_dtls(array("upload_id"=>$upload_id, "user_id"=>$uid));
		$data['title'] = "Profile";
		$data['body'] = "view_emp_salary_dtl";
		$this->load->view('common/structure',$data);	
	}	
	
	
	
}
