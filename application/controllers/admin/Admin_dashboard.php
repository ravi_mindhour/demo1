<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
		//$this->load->library('parser');
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("admin_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	public function index()
	{
		$data['msg'] = "";		
		$data['staff_list'] = $this->admin_model->get_staff_list();				
		$data['title'] = "Staffs";
		$data['body'] = "admin/staff_list";
		$this->load->view('common/structure',$data);		
	}

	public function add_staff()
	{
		$data['msg'] = "";		

		if($this->input->post())
		{
			$name = $this->input->post('txt_name');						
			$email = $this->input->post('txt_email');
			$pwd = $this->input->post('txt_pwd');
			$designation = $this->input->post('ddl_designation');
			$status = $this->input->post('ddl_status');	
			
			$staff_db_arr = array(
							"name"=>$name,
							"email"=>$email,
							"pass"=>md5($pwd),
							"status"=>$status,
							"desig"=>$designation,
							"company_Id"=>$this->session->userdata('companyid_ses'));
			$this->admin_model->insert_staff($staff_db_arr);
			$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>New staff member created successfully.</b></span></div>'); 
			redirect(site_url("add-staff"));
		}

		$data['designation_list'] = $this->admin_model->get_designation_list();				
		$data['title'] = "Add Staffs";
		$data['body'] = "admin/add_staff";
		$this->load->view('common/structure',$data);		
	}

	public function country_list()
	{
		$data['msg'] = "";		
		$data['country_list'] = $this->admin_model->get_country_list();				
		$data['title'] = "Country";
		$data['body'] = "admin/country_list";
		$this->load->view('common/structure',$data);		
	}	

	public function add_country()
	{
		$data['msg'] = "";		

		if($this->input->post())
		{
			$name = $this->input->post('txt_name');						
			$status = $this->input->post('ddl_status');	
			
			$db_arr = array(
						"name"=>$name,
						"status"=>$status);
			$this->admin_model->insert_country($db_arr);
			$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>New country created successfully.</b></span></div>'); 
			redirect(site_url("add-country"));
		}
			
		$data['title'] = "Add Country";
		$data['body'] = "admin/add_country";
		$this->load->view('common/structure',$data);		
	}

	public function business_attributes_list()
	{
		$data['msg'] = "";		
		$data['business_attributes_list'] = $this->admin_model->get_business_attributes_list();				
		$data['title'] = "Business Attributes";
		$data['body'] = "admin/business_attributes_list";
		$this->load->view('common/structure',$data);		
	}

	public function designation_list()
	{
		$data['msg'] = "";		
		//$data['designation_list'] = $this->admin_model->get_designation_list();				
		$data['title'] = "Designation";
		$data['body'] = "admin/designation_list";
		$this->load->view('common/structure',$data);		
	}

	public function manage_grade()
	{
		$data['msg'] = "";		
		//$data['designation_list'] = $this->admin_model->get_designation_list();				
		$data['title'] = "Manage Grade";
		$data['body'] = "admin/manage_grade";
		$this->load->view('common/structure',$data);		
	}

	public function manage_city()
	{
		$data['msg'] = "";		
		//$data['designation_list'] = $this->admin_model->get_designation_list();				
		$data['title'] = "Manage City";
		$data['body'] = "admin/manage_city";
		$this->load->view('common/structure',$data);		
	}

}
