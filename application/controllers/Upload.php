<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');
		$this->load->model("upload_model");
		$this->load->model("approvel_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	/*public function index()
	{
		$data['msg'] = "";						
		$data['title'] = "Performance Cycle";
		$data['body'] = "performance_cycle";
		$this->load->view('common/structure',$data);
	}*/
	
	public function performance_cycle()
	{
		$data['msg'] = "";		
		$user_id = $this->session->userdata("userid_ses");
		if($this->input->post())
		{
			$performance_cycle_name = $this->input->post('txt_performance_cycle_name');						
			$description = $this->input->get_post('txt_description');
			$currentDateTime = date("Y-m-d H:i:s");	

			$start_dt=explode("/",$this->input->post('txt_start_dt')); 
			$start_date=($start_dt[2].'-'.$start_dt[1].'-'.$start_dt[0]); 	
			$end_dt=explode("/",$this->input->post('txt_end_dt')); 
			$end_date=($end_dt[2].'-'.$end_dt[1].'-'.$end_dt[0]); 		
			
			$performance_cycle_db_arr = array(
										"name"=>$performance_cycle_name,
										"start_date"=>$start_date,
										"end_date"=>$end_date,
										"description"=>$description,
										"createdby"=>$user_id,
										"createdon"=>$currentDateTime,
										"updatedby"=>$user_id,
										"updatedon"=>$currentDateTime);
			$performance_cycle_id = $this->upload_model->insert_performance_cycle($performance_cycle_db_arr);
			$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>Performance cycle created successfully.</b></span></div>'); 
			
			redirect(site_url("performance-cycle"));
		}
		

		$data["performance_cycle_list"] = $this->upload_model->get_performance_cycles(array("createdby"=>$user_id));

		//$this->load->model("approvel_model");
		$data['approvel_request_list'] = $this->approvel_model->get_approvel_request_list($user_id);
		$data['rules_approvel_request_list'] = $this->approvel_model->get_rules_approvel_request_list($user_id);	
		$data['title'] = "Performance Cycle";
		$data['body'] = "performance_cycle";
		$this->load->view('common/structure',$data);
	}
	
	public function upload_data($performance_cycle_id)
	{	
		if($_FILES)
		{
			$user_id = $this->session->userdata("userid_ses");
			$user_email = $this->session->userdata("email_ses");
			$currentDateTime = date("Y-m-d H:i:s");
			$file_type = explode('.',$_FILES['myfile']['name']);

			if(($_FILES['myfile']['type'] == 'text/csv' or $_FILES['myfile']['type'] == 'text/comma-separated-values' or $_FILES['myfile']['type'] == 'application/vnd.ms-excel') and $file_type[1] == 'csv')
			{	
				$fName = time().".".$file_type[1];//$_FILES["myfile"]["name"];
				$dd = move_uploaded_file($_FILES["myfile"]["tmp_name"],  "uploads/".$fName);
				
				// Name of your CSV file
				$csv_file = base_url()."uploads/".$fName;				
				
				if(($handle = fopen($csv_file, "r")) !== FALSE)
				{
					// fgetcsv($handle);  
					$header=fgetcsv($handle, ",");//Get header only from uploaded file
					$total_column = count($header);
					/*while(($data = fgetcsv($handle,0,  ",")) !== FALSE)
					{
						$num = count($data);
						for ($c=0; $c < $num; $c++)
						{
							$col[$c] = $data[$c];
						}				
					}*/
					
					$upload_db_arr = array(
										"original_file_name"=>$fName,
										"upload_date"=>$currentDateTime,
										"uploaded_by_user_id"=>$user_id,
										"uploaded_by_role_id"=>1,
										"template_id"=>1,
										"data_upload_status_code"=>1,
										"client_id"=>14,
										"performance_cycle_id"=>$performance_cycle_id);
					$upload_id=$this->upload_model->insert_uploaded_file_dtls($upload_db_arr);
											
					/*for($i=0;$i<count($header);$i++)
					{
						$bussiness_attri_db_arr = array(
													"display_name"=>$header[$i],
													"description"=>'',
													"cardinal_value"=>0,
													"dependent_on_attribute_id"=>1,
													"css_class"=>'',
													"data_type_code"=>'VARCHA',
													"upload_id"=>$upload_id);						
						$this->upload_model->insert_Bussiness_Attri($bussiness_attri_db_arr);
					}
					
					$tuples_db_arr = array(
										"row_num"=>$total_column,
										"data_upload_id"=>$upload_id,
										"tuple_status_code"=>1,
										"updated_by_user_id"=>$user_id);
					$id = $this->upload_model->insert_tuples($tuples_db_arr);*/
					$this->upload_model->update_performance_cycle_status($upload_id, $this->session->userdata("userid_ses"), 2);
					redirect(site_url("mapping-head/".$upload_id));
				}
				else
				{
					echo $data['msg'] = "Please upload correct file (*.csv)..";die;					
				}
			}
			else
			{
				echo $data['msg'] = "Please upload correct file (*.csv).e";die;
			}
		}
				
		$data['performance_cycle_id'] = $performance_cycle_id;
		$data['title'] = "Upload Data";
		$data['body'] = "upload_file";
		$this->load->view('common/structure',$data);
	}
	
	public function mapping_headers($upload_id=0)
	{
		$uploaded_file_dtl= $this->upload_model->get_uploaded_file_dtls($upload_id) ;
		$csv_file = base_url()."uploads/".$uploaded_file_dtl["original_file_name"];
		if(($handle = fopen($csv_file, "r")) !== FALSE)
		{
			$data['header_list'] = fgetcsv($handle, ",");//Get header only from uploaded file
			//echo "<pre>"; print_r($header);die;
			$this->load->model("business_attribute_model");
			$data['business_attribute_list']=$this->business_attribute_model->get_business_attributes(array("type"=>CV_BA_SALARY_TYPE));
		}		
		$data['upload_id'] = $upload_id;		
		$data['title'] = "Mapping Headers";
		$data['body'] = "mapping_headers";
		$this->load->view('common/structure',$data);
	}
	
	public function insert_headers()
	{	
		$data1 = array();
		$upload_id = $this->input->post('hf_upload_id');
		$uploaded_file_dtl= $this->upload_model->get_uploaded_file_dtls($upload_id) ;
			
		$csv_file = base_url()."uploads/".$uploaded_file_dtl["original_file_name"];
		/*$module = $this->input->post('module');
		$dataType = $this->input->post('dataType');
		$map = $this->input->post('map');*/
		
		// Name of your CSV file
		$row = 0 ;

		if(($handle = fopen($csv_file, "r")) !== FALSE)
		{
			fgetcsv($handle);  
			
			/*$str = $this->input->post('newValue');
			$ids = $this->input->post('ids');
			$bId = 0;
			$dd = explode(",",$str);
			$module = explode(",",$module);
			$dataType = explode(",",$dataType);
			$map = explode(",",$map);
			$map1 = array(2);
			
			for($i=0;$i<count($map);$i++)
			{
				$map1 = explode(";",$map[$i]);
				$map[$i] = $map1[0];				
				$data_type[$i] = @$map1[1];			
			}
				
			$this->load->model('data_type_model');*/
			$this->load->model('messages_model');
			//$this->load->model('approvel_model');
			/*for($i = 0;$i<count($ids); $i++)
			{
				$this->upload_model->update_Bussiness_Attri($map[$i],$ids[$i]);
			}*/
			//echo $_REQUEST['ddl_mapping'][0]."<br\>".$_REQUEST['ddl_mapping'][1]."<br\>".$_REQUEST['ddl_mapping'][2];die;
			while(($data = fgetcsv($handle,0,  ",")) !== FALSE)//Row loop start
			{
				$num = count($data); // total column in a row				
				$rowOwner_db_arr = array(
									"row_id"=>$row,
									"upload_id"=>$upload_id,
									"first_approver"=>"",
									"second_approver"=>"",
									"third_approver"=>"",
									"fourth_approver"=>"");
				for ($c=0; $c < $num; $c++)//Column loop start
				{
					if($_REQUEST['ddl_mapping'][$c] != 'N/A')
					{	
						$mapped_arr = explode(CV_CONCATENATE_SYNTAX,$_REQUEST['ddl_mapping'][$c]);
						
						$mapped_id = $_REQUEST['hf_attribute_ids'][$c];
						$mapped_name = "N/A";
						if(count($mapped_arr)>1)
						{
							$mapped_id = $mapped_arr[0];				
							$mapped_name = @$mapped_arr[1];	
						}
						
	
						$col[$c] = $data[$c];					
						/*if($bId >= count($ids))
						{
							$bId = 0;
						}*/
	
						//$dataType =  $this->data_type_model->myGetType($col[$c]);
						/*if($data_type[$c] == $dataType)
						{*/				
							/*$data1["list"][$c] = $this->upload_model->check_module($map[$bId],$col[$c],$upload_id);
							if($map[$bId] == "branch" || $map[$bId] == "Branch")
							{
								$this->upload_model->setUserBranch($upload_id,$row,$col[$c]);
							}*/
							
							$datum_db_arr = array(
											"row_num"=>$row,
											"col_num"=>$c,
											"data_upload_id"=>$upload_id,
											"display_name_override"=>$_REQUEST['txt_head_name'][$c],
											"uploaded_value"=>$col[$c],
											"value"=>$col[$c],
											"datum_status_code"=>"",
											"business_attribute_id"=>$mapped_id
											);
											
							$data_maping_db_arr = array(
													"upload_id"=>$upload_id,
													"business_attribut"=>$mapped_id,
													"mapped"=>$mapped_name);
							/*if($c == 0)
							{
								$rowOwner_db_arr["first_approver"] = $col[$c];
							}
							if($c == 1)
							{
								$rowOwner_db_arr["second_approver"] = $col[$c];
							}
							if($c == 2)
							{
								$rowOwner_db_arr["third_approver"] = $col[$c];
							}
							if($c == 3)
							{
								$rowOwner_db_arr["fourth_approver"] = $col[$c];
							}*/						
							$this->upload_model->insertHeaders($datum_db_arr,$data_maping_db_arr);
							
						/*}				
						else
						{
							$data1['error'][$c]="error in insertion at row=". $row." col=". $c . "\n" ;
							$msg_db_arr = array(
											"message_text"=>"error in insertion",
											"message_definition_message_num"=>0,
											"message_type_code"=>"WAR",
											"data_upload_id"=>$upload_id,
											"datum_col_num"=>$c,
											"datum_row_num"=>$row,
											"tuple_row_num"=>$this->messages_model->getTuple($upload_id));						
							$this->messages_model->insertMessage($msg_db_arr);					
						}	*/
					}
				}
				$this->upload_model->insertRowOwner($rowOwner_db_arr);
				$tuples_db_arr = array(
									"row_num"=>$row,
									"data_upload_id"=>$upload_id,
									"tuple_status_code"=>1,
									"updated_by_user_id"=>$this->session->userdata("userid_ses"));
				$id = $this->upload_model->insert_tuples($tuples_db_arr);
				$row++;			
			}
			$this->approvel_model->create_approvel_req($upload_id, $this->session->userdata("userid_ses"));
			$this->upload_model->update_performance_cycle_status($upload_id, $this->session->userdata("userid_ses"), 3);
			$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>Header mapped successfully.</b></span></div>'); 
			echo true;				
		}	
	}
	
	public function show_uploaded_data($upload_id)
	{	
		$is_enable_approve_btn = 0; // No
		$request_dtl= $this->approvel_model->check_approvel_request_is_valid_for_user(array("upload_id"=>$upload_id, "approvel_requests.user_id"=>$this->session->userdata("userid_ses"),"approvel_requests.status"=>0, "approvel_requests.type"=>1));
		if($request_dtl)
		{
			$is_enable_approve_btn = 1; //Yes
		}

		/*$is_enable_approve_btn = 1; // Yes
		$uploaded_file_dtl= $this->upload_model->get_uploaded_file_dtls($upload_id);
		if($uploaded_file_dtl["uploaded_by_user_id"] == $this->session->userdata('userid_ses'))
		{
			$is_enable_approve_btn = 0; //No
		}
		
		$this->load->model("business_attribute_model");
		$headers = $this->business_attribute_model->get_business_attributes();*/
		$result = $this->upload_model->fetchDatum($upload_id);
		$headers = $this->upload_model->fetchHeaders($upload_id);
		$data = array("headers"=>$headers,"datum"=>	$result);
		$data['upload_id'] = $upload_id;
		$data['is_enable_approve_btn'] = $is_enable_approve_btn;		
		$data['title'] = "Uploaded Data";
		$data['body'] = "show_uploaded_data";
		//echo "<pre>";print_r($data["datum"]);die;
		$this->load->view('common/structure',$data);
	}

	//------------------------------------ Upload Employee Data ---------------------------

	public function upload_employee()
	{	
		if($_FILES)
		{
			$user_id = $this->session->userdata("userid_ses");
			$user_email = $this->session->userdata("email_ses");
			$currentDateTime = date("Y-m-d H:i:s");
			$file_type = explode('.',$_FILES['myfile']['name']);			

			if(($_FILES['myfile']['type'] == 'text/csv' or $_FILES['myfile']['type'] == 'text/comma-separated-values' or $_FILES['myfile']['type'] == 'application/vnd.ms-excel') and $file_type[1] == 'csv')
			{	
				$fName = "imp_employee/".time().".".$file_type[1];//$_FILES["myfile"]["name"];
				$dd = move_uploaded_file($_FILES["myfile"]["tmp_name"],  "uploads/".$fName);
				$csv_file = base_url()."uploads/".$fName;				

				if(($handle = fopen($csv_file, "r")) !== FALSE)
				{ 
					$header=fgetcsv($handle, ",");//Get header only from uploaded file
					$total_column = count($header);					
					$upload_db_arr = array(
										"original_file_name"=>$fName,
										"upload_date"=>$currentDateTime,
										"uploaded_by_user_id"=>$user_id,
										"uploaded_by_role_id"=>1,
										"template_id"=>1,
										"data_upload_status_code"=>1,
										"client_id"=>14);
					$upload_id=$this->upload_model->insert_uploaded_file_dtls($upload_db_arr);
					redirect(site_url("mapping-employee-head/".$upload_id));
				}
				else
				{
					echo $data['msg'] = "Please upload correct file (*.csv)..";die;					
				}
			}
			else
			{
				echo $data['msg'] = "Please upload correct file (*.csv).e";die;
			}
		}
				
		$data['title'] = "Upload Employee";
		$data['body'] = "upload_file";
		$this->load->view('common/structure',$data);
	}

	public function mapping_employee_headers($upload_id=0)
	{
		if($this->input->post())
		{
			$tuples_arr = $this->upload_model->get_tuples(array("data_upload_id"=>$upload_id));	

			if($upload_id==$this->input->post("hf_upload_id") and (!count($tuples_arr)))
			{
				$upload_id = $this->input->post('hf_upload_id');
				$uploaded_file_dtl= $this->upload_model->get_uploaded_file_dtls($upload_id) ;
				$csv_file = base_url()."uploads/".$uploaded_file_dtl["original_file_name"];
				
				// Name of your CSV file
				$row = 0 ;
				if(($handle = fopen($csv_file, "r")) !== FALSE)
				{
					fgetcsv($handle);  				
					//$this->load->model('approvel_model');

					while(($data = fgetcsv($handle,0,  ",")) !== FALSE)//Row loop start
					{
						$num = count($data); // total column in a row				
						

						$tuples_db_arr = array(
										"row_num"=>$row,
										"data_upload_id"=>$upload_id,
										"tuple_status_code"=>1,
										"updated_by_user_id"=>$this->session->userdata("userid_ses"));
						$this->upload_model->insert_tuples($tuples_db_arr);

						for ($c=0; $c < $num; $c++)//Column loop start
						{
							if($_REQUEST['ddl_mapping'][$c])
							{
								$mapped_arr = explode(CV_CONCATENATE_SYNTAX,$_REQUEST['ddl_mapping'][$c]);
								$mapped_id = $mapped_arr[0];				
								$mapped_name = @$mapped_arr[1];									
								$col[$c] = $data[$c];								
								$datum_db_arr = array(
												"row_num"=>$row,
												"col_num"=>$c,
												"data_upload_id"=>$upload_id,
												"display_name_override"=>$_REQUEST['txt_head_name'][$c],
												"uploaded_value"=>$col[$c],
												"value"=>$col[$c],
												"datum_status_code"=>"",
												"business_attribute_id"=>$mapped_id
												);												
								$data_maping_db_arr = array(
														"upload_id"=>$upload_id,
														"business_attribut"=>$mapped_id,
														"mapped"=>$mapped_name);
												
								$this->upload_model->insertHeaders($datum_db_arr,$data_maping_db_arr);
								$this->upload_model->check_module($mapped_name, $col[$c], $row, $upload_id);
							}							
						}
						$row++;			
					}
/*					$added_user_cnt = $this->upload_model->get_tuples(array("data_upload_id"=>$upload_id));
					if($added_user_cnt)
					{
						$this->session->set_flashdata('message', '<div align="left" style="color:blue;" id="notify"><span><b>'.count($added_user_cnt).' employee created successfully.</b></span></div>'); 
					}*/	
					$this->session->set_flashdata('flash_upload_id', $upload_id); 
					redirect(site_url("staff"));
				}
			}
			else
			{
				redirect(site_url("dashboard"));
			}
		}
		$uploaded_file_dtl= $this->upload_model->get_uploaded_file_dtls($upload_id) ;
		$csv_file = base_url()."uploads/".$uploaded_file_dtl["original_file_name"];
		if(($handle = fopen($csv_file, "r")) !== FALSE)
		{
			$data['header_list'] = fgetcsv($handle, ",");//Get header only from uploaded file
			$this->load->model("business_attribute_model");
			$data['business_attribute_list']=$this->business_attribute_model->get_business_attributes(array("type"=>CV_BA_EMPLOYE_TYPE));
		}		
		$data['upload_id'] = $upload_id;		
		$data['title'] = "Mapping Headers";
		$data['body'] = "mapping_employee_headers";
		$this->load->view('common/structure',$data);
	}






	
}
