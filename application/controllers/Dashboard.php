<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
		//$this->load->library('parser');
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("front_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Admin' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	public function index()
	{
		$data['msg'] = "";
						
		$data['title'] = "Dashboard";
		$data["statics"] = $this->front_model->get_dashboard_statics();
		$data['body'] = "dashboard";
		$this->load->view('common/structure',$data);
		
		/*$data = array('title'=> 'Home',
        'body' =>$this->parser->parse('index',array(),true));
        $this->parser->parse('common/structure', $data);*/
	}

	public function profile()
	{
		$data['msg'] = "";						
		$data['title'] = "Profile";
		$data['body'] = "profile";
		$this->load->view('common/structure',$data);		
	}

}
