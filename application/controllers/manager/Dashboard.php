<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		
		//$this->load->library('parser');
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->library('session', 'encrypt');	
		$this->load->model("manager_model");
		$is_correct_role_n_DB = false;
		if($this->session->userdata('role_ses') == 'Manager' and $this->session->userdata('dbname_ses') != '')
		{	
			$is_correct_role_n_DB=true;
		}
		
		if(!($this->session->userdata('userid_ses')) or ($is_correct_role_n_DB != true))
		{			
			redirect(site_url("logout"));			
		}                       
    }
	
	public function index()
	{
		$this->performance_cycle();		
	}

	public function profile()
	{
		$data['msg'] = "";						
		$data['title'] = "Profile";
		$data['body'] = "profile";
		$this->load->view('manager/common/structure',$data);		
	}

	public function performance_cycle()
	{
		$data['msg'] = "";		
		$data["performance_cycle_list"] = $this->manager_model->get_performance_cycles_list();	
		$data['title'] = "Performance Cycle";
		$data['body'] = "manager/performance_cycle_list";
		$this->load->view('manager/common/structure',$data);
	}

	public function view_increments_list($performance_cycle_id)
	{
		$data["performance_cycle_list"] = $this->manager_model->get_performance_cycles(array("id"=>$performance_cycle_id, "status"=>7));
		$data['title'] = "View Increment";
		$data['body'] = "manager/increment_index";
		$this->load->view('manager/common/structure',$data);
	}

	public function get_employee_list()
	{
		if($this->input->post("pid"))
		{		
			$data["performance_cycle_id"] = $this->input->post("pid");
			$upload_dtl_arr = $this->manager_model->get_upload_id_by_performance_cycle_id(array("performance_cycle_id"=>$data["performance_cycle_id"]));			
			$data['upload_id'] = $upload_dtl_arr["id"];
			$data['staff_list'] = $this->manager_model->get_employees_list_by_uploaded_id($upload_dtl_arr["id"]);	
			$this->load->view('manager/increment_partial',$data);
		}
	}

	public function view_employee_increment($upload_id, $uid)
	{	
		$data['salary_dtls'] = $this->manager_model->get_employee_salary_dtls(array("upload_id"=>$upload_id, "user_id"=>$uid));
		$data['title'] = "Profile";
		$data['body'] = "view_emp_salary_dtl";
		$this->load->view('manager/common/structure',$data);	
	}	

}
