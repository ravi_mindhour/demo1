<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ravi extends CI_Controller 
{	 
	 public function __construct()
	 {		
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		$this->load->library('session', 'encrypt');			
		$this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');                              
    }
	
	public function index()
	{
		$data['msg'] = "";		
		$data['title'] = "Dashboard";
		$data['body'] = "manager_dashboard";
		$this->load->view('common/structure',$data);
	}
	
	public function rules()
	{
		$data['msg'] = "";		
		$data['title'] = "Rules";
		$data['body'] = "create_rules";
		$this->load->view('common/structure',$data);
	}
	
	
}
